INSERT INTO PRODUCT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (1,'Alimentos',null, now(), now());
INSERT INTO PRODUCT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (2,'Medicamentos e Saúde',null, now(), now());
INSERT INTO PRODUCT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (3,'Higiene e beleza',null, now(), now());
INSERT INTO PRODUCT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (4,'Brinquedos',null, now(), now());
INSERT INTO PRODUCT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (5,'Passeio e viagem',null, now(), now());
INSERT INTO PRODUCT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (6,'Conforto',null, now(), now());
INSERT INTO PRODUCT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (7,'Outros',null, now(), now());

INSERT INTO ANIMAL_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (1,'Gatos',null, );
INSERT INTO ANIMAL_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (2,'Cachorros',null, now(), now());
INSERT INTO ANIMAL_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (3,'Passaros',null, now(), now());
INSERT INTO ANIMAL_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (4,'Peixes',null, now(), now());
INSERT INTO ANIMAL_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (5,'Repteis',null, now(), now());
INSERT INTO ANIMAL_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (6,'Outros',null, now(), now());

INSERT INTO AGE_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (1,'Recém Nascido',null, now(), now());
INSERT INTO AGE_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (2,'Filhote',null, now(), now());
INSERT INTO AGE_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (3,'Adulto',null, now(), now());
INSERT INTO AGE_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (4,'Idoso',null, now(), now());
INSERT INTO AGE_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (5,'Todas',null, now(), now());

INSERT INTO MEASUREMENT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (1,'Quilograma',null, now(), now());
INSERT INTO MEASUREMENT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (1,'Grama',null, now(), now());
INSERT INTO MEASUREMENT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (1,'Miligrama',null, now(), now());
INSERT INTO MEASUREMENT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (1,'Litros',null, now(), now());
INSERT INTO MEASUREMENT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (1,'Tamanho',null, now(), now());
INSERT INTO MEASUREMENT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (1,'Outros',null, now(), now());






