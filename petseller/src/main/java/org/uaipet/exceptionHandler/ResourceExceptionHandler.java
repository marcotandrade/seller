package org.uaipet.exceptionHandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.uaipet.exception.*;
import org.uaipet.service.exceptions.AddressException;
import org.uaipet.service.exceptions.AuthorizationException;
import org.uaipet.service.exceptions.OutOfDeliveryRangeException;
import org.uaipet.service.exceptions.ProductFromDifferentStoreException;
import org.uaipet.service.exceptions.ProductUnavailableException;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ResourceExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceExceptionHandler.class);

    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler(CustomerException.class)
    public ResponseEntity CustomerExceptionHandler(CustomerException ex, HttpServletRequest request) {

        LOGGER.error("CustomerException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.NOT_FOUND.value())
                        .message(messageSource.getMessage("custumer.email.already.exists", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );

    }

    @ExceptionHandler(CustomerAlreadyExistsException.class)
    public ResponseEntity CustomerAlreadyExistsExceptionHandler(CustomerAlreadyExistsException ex, HttpServletRequest request) {

        LOGGER.error("CustomerAlreadyExistsException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.NOT_FOUND.value())
                        .message(messageSource.getMessage("custumer.email.already.exists", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );

    }


    @ExceptionHandler(FileSavingException.class)
    public ResponseEntity FileSavingExceptionHandler(FileSavingException ex, HttpServletRequest request) {

        LOGGER.error("FileSavingException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.NOT_FOUND.value())
                        .message(messageSource.getMessage("file.saving.error", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );

    }



    @ExceptionHandler(CustomerNotFoundException.class)
    public ResponseEntity CustomerNotFountException(CustomerNotFoundException ex, HttpServletRequest request) {

        LOGGER.error("CustomerNotFountException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.NOT_FOUND.value())
                        .message(messageSource.getMessage("custumer.not.found", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );

    }

    @ExceptionHandler(AddressException.class)
    public ResponseEntity AddressException(AddressException ex, HttpServletRequest request) {

        LOGGER.error("AddressException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.BAD_REQUEST.value())
                        .message(messageSource.getMessage("custumer.address.information", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }
    @ExceptionHandler(InvalidAddressException.class)
    public ResponseEntity InvalidAddressExceptionHandler(InvalidAddressException ex, HttpServletRequest request) {

        LOGGER.error("InvalidAddressException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.BAD_REQUEST.value())
                        .message(messageSource.getMessage("invalid.address", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }

    @ExceptionHandler(AuthorizationException.class)
    public ResponseEntity AuthorizationException(AuthorizationException ex, HttpServletRequest request) {

        LOGGER.error("AuthorizationException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(messageSource.getMessage("user.unauthorized", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }


    @ExceptionHandler(CompanyAlreadyExistsException.class)
    public ResponseEntity CompanyAlreadyExistsException(CompanyAlreadyExistsException ex, HttpServletRequest request) {

        LOGGER.error("CompanyAlreadyExistsException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(messageSource.getMessage("company.already.exists", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }

    @ExceptionHandler(UserNotAssociatedWithCompanyException.class)
    public ResponseEntity UserNotAssociatedWithCompanyExceptionHandler(UserNotAssociatedWithCompanyException ex, HttpServletRequest request) {

        LOGGER.error("UserNotAssociatedWithCompanyExceptionHandler: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(messageSource.getMessage("company.not.associated.to.company", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }

    @ExceptionHandler(ProductFromDifferentStoreException.class)
    public ResponseEntity ProductFromDifferentStoreException(ProductFromDifferentStoreException ex, HttpServletRequest request) {

        LOGGER.error("ProductFromDifferentStoreException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(messageSource.getMessage("product.from.different.store", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }

    @ExceptionHandler(ProductUnavailableException.class)
    public ResponseEntity ProductUnavailableException(ProductUnavailableException ex, HttpServletRequest request) {

        LOGGER.error("ProductUnavailableException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(messageSource.getMessage("product.unavailable", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }

    @ExceptionHandler(OutOfDeliveryRangeException.class)
    public ResponseEntity OutOfDeliveryRangeException(OutOfDeliveryRangeException ex, HttpServletRequest request) {

        LOGGER.error("ProductUnavailableException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(messageSource.getMessage("address.out.of.delivery.range", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }

    @ExceptionHandler(CompanyException.class)
    public ResponseEntity CompanyException(CompanyException ex, HttpServletRequest request) {

        LOGGER.error("CompanyException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(messageSource.getMessage("company.already.exists", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );

    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity ExceptionHandler(Exception ex, HttpServletRequest request) {

        LOGGER.error("Exception: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                        .message(messageSource.getMessage("petseller.internal.error", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );

    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity UserNotFountException(UserNotFoundException ex, HttpServletRequest request) {

        LOGGER.error("UserNotFoundException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.NOT_FOUND.value())
                        .message(messageSource.getMessage("user.not.found", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );

    }

    @ExceptionHandler(CompanyNotFoundException.class)
    public ResponseEntity CompanyNotFoundException(CompanyNotFoundException ex, HttpServletRequest request) {

        LOGGER.error("CompanyNotFoundException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.NOT_FOUND.value())
                        .message(messageSource.getMessage("company.not.found", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );

    }


    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity AccessDeniedException(AccessDeniedException ex, HttpServletRequest request) {

        LOGGER.error("AccessDeniedException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(messageSource.getMessage("resource.operation-not-allowed", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );

    }


}
