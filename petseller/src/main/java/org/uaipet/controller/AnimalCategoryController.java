package org.uaipet.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.uaipet.data.model.AnimalCategory;
import org.uaipet.data.repository.AnimalCategoryRepository;

@RestController
public class AnimalCategoryController {

    private static final Logger log = LoggerFactory.getLogger(AnimalCategoryController.class);

    @Autowired
    private AnimalCategoryRepository animalCategoryRepository;

    @GetMapping(value = "/findAll/AnimalCategory")
    public ResponseEntity<Iterable<AnimalCategory>> getAllAnimalCategories(){
        long startTime = System.currentTimeMillis();
        log.info("getAllAnimalCategories initialized!!");
        Iterable<AnimalCategory> animalCategories = animalCategoryRepository.findAll();
        log.info(String.format("getAllAnimalCategories Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return ResponseEntity.ok(animalCategories);

    }



}
