package org.uaipet.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.uaipet.data.model.AgeCategory;
import org.uaipet.data.repository.AgeCategoryRepository;

@RestController
public class AgeCategoryController {

    private static final Logger log = LoggerFactory.getLogger(AgeCategoryController.class);

    @Autowired
    private AgeCategoryRepository ageCategoryRepository;

    @GetMapping(value = "/findAll/AgeCategory")
    public ResponseEntity<Iterable<AgeCategory>> getAllAgeCategories(){
        long startTime = System.currentTimeMillis();
        log.info("getAllAgeCategories initialized!!");
        Iterable<AgeCategory> ageCategories = ageCategoryRepository.findAll();
        log.info(String.format("getAllAgeCategories Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return ResponseEntity.ok(ageCategories);

    }



}
