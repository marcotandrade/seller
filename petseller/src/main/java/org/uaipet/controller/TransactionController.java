package org.uaipet.controller;


import org.uaipet.exception.TransactionException;
import org.uaipet.data.model.Transaction;
import org.uaipet.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
public class TransactionController {

    private static final Logger log = LoggerFactory.getLogger(TransactionController.class);

    @Autowired
    private TransactionService transactionService;

    /**
     * search for all transactions of the merchant between the informed period
     *
     * @param merchantId
     * @param beginDate
     * @param endDate
     * @return
     */
    @GetMapping(value = "/transaction/{merchantId}/{beginDate}/{endDate}")
    public ResponseEntity<List<Transaction>> getMerchantTransactions(@PathVariable Long merchantId,
                                                                 @PathVariable String beginDate,
                                                                 @PathVariable String endDate) {
        ResponseEntity response;
        long startTime = System.currentTimeMillis();
        log.info("getMerchantTransactions Inicializado!!");
        try {
            Date bDate = new SimpleDateFormat("yyyy-MM-dd").parse(beginDate);
            Date fDate = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);

            Calendar cal = Calendar.getInstance();
            cal.setTime(fDate);
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);

            List<Transaction> merchantTranList = transactionService.listUnPayedTransactions(merchantId, bDate, cal.getTime());
            response = new ResponseEntity<>(merchantTranList, HttpStatus.OK);
        } catch (ParseException | TransactionException ex) {
            response = new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        log.info(String.format("getMerchantTransactions Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return response;
    }





}
