package org.uaipet.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.uaipet.data.model.DeliveryArea;
import org.uaipet.data.model.Company;
import org.uaipet.data.repository.DeliveryAreaRepository;
import org.uaipet.data.repository.CompanyRepository;
import org.uaipet.dto.request.DeliveryAreaRequest;
import org.uaipet.service.DeliveryAreaService;
import org.uaipet.service.SearchIndexService;
import org.uaipet.service.UserService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class DeliveryAreaController {

    private static final Logger log = LoggerFactory.getLogger(DeliveryAreaController.class);

    @Autowired
    private DeliveryAreaRepository deliveryAreaRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private SearchIndexService searchIndexService;

    @Autowired
    private DeliveryAreaService deliveryAreaService;

    @Autowired
    private UserService userService;

    @PutMapping(value = "/deliveryArea")
    public ResponseEntity<Boolean> addDeliveryArea(@RequestBody DeliveryAreaRequest deliveryArea) {
        ResponseEntity response;
        long startTime = System.currentTimeMillis();
        log.info("addDeliveryArea initialized!!");
        userService.requestUserIsAssociatedWithCompany(deliveryArea.getUserId(), deliveryArea.getCompanyId());
        return ResponseEntity.ok(deliveryAreaService.registerDeliveryArea(deliveryArea));
    }


    @GetMapping(value = "/deliveryAreaFromCompany/{companyId}")
    public ResponseEntity<DeliveryArea> getDeliveryAreaFromCompany(@RequestBody DeliveryAreaRequest deliveryAreaRequest) {
        ResponseEntity response;
        long startTime = System.currentTimeMillis();
        log.info("getDeliveryAreaFromCompany initialized!!");
        try {
            userService.requestUserIsAssociatedWithCompany(deliveryAreaRequest.getUserId(), deliveryAreaRequest.getCompanyId());
            Optional<Company> mer = companyRepository.findById(deliveryAreaRequest.getCompanyId());
            List<DeliveryArea> deliveryArea = new ArrayList<>();
            if (mer.isPresent()) {
                deliveryArea = deliveryAreaRepository.findByCompany(mer.get());
                deliveryArea.forEach( area -> area.setCompany(null));
            }
            response = new ResponseEntity<>(deliveryArea, HttpStatus.OK);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            response = new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        log.info(String.format("getDeliveryAreaFromCompany finalized in %s ms!", System.currentTimeMillis() - startTime));
        return response;
    }

    @DeleteMapping(value = "/deliveryArea")
    public ResponseEntity<String> deleteDeliveryAreaFromCompany(@RequestBody DeliveryAreaRequest deliveryArea) {
        log.info("deleteDeliveryAreaFromCompany initialized!!");
        userService.requestUserIsAssociatedWithCompany(deliveryArea.getUserId(), deliveryArea.getCompanyId());
        deliveryAreaService.deleteDeliveryArea(deliveryArea.getCompanyId());
        return ResponseEntity.ok("Área de delivery deletada");
    }


}
