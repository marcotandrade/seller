package org.uaipet.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.uaipet.data.model.MeasurementCategory;
import org.uaipet.data.repository.MeasurementCategoryRepository;

@RestController
public class MeasurementCategoryController {

    private static final Logger log = LoggerFactory.getLogger(MeasurementCategoryController.class);

    @Autowired
    private MeasurementCategoryRepository measurementCategoryRepository;

    @GetMapping(value = "/findAll/MeasurementCategories")
    public ResponseEntity<Iterable<MeasurementCategory>> getAllMeasurementCategories(){
        long startTime = System.currentTimeMillis();
        log.info("getAllMeasurementCategories initialized!!");
        Iterable<MeasurementCategory> measurementCategories = measurementCategoryRepository.findAll();
        log.info(String.format("getAllMeasurementCategories Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return ResponseEntity.ok(measurementCategories);

    }



}
