package org.uaipet.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.uaipet.data.model.User;
import org.uaipet.dto.AddressDTO;
import org.uaipet.dto.CompanyDTO;
import org.uaipet.dto.request.FreightValueRequest;
import org.uaipet.dto.request.CompanyRequest;
import org.uaipet.dto.request.UserRequest;
import org.uaipet.exception.CompanyAlreadyExistsException;
import org.uaipet.exception.CompanyException;
import org.uaipet.data.model.Company;
import org.uaipet.data.repository.CompanyRepository;
import org.uaipet.security.JWTUtil;
import org.uaipet.service.CompanyService;
import org.uaipet.service.SearchIndexService;
import org.uaipet.utils.InputFilter;
import org.uaipet.utils.ProcessResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/company")
public class CompanyController {

    private static final Logger log = LoggerFactory.getLogger(CompanyController.class);

    @Autowired
    private CompanyService companyService;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private InputFilter inputFilter;

    @Autowired
    private SearchIndexService searchIndexService;

    @Autowired
    private JWTUtil jwtUtil;

    /**
     * Creates the Company
     *
     * @return
     */
    @PostMapping
    @ApiOperation("Cadastrar loja")
    @ResponseBody
    public ResponseEntity addCompany(@RequestParam("company") CompanyRequest companyRequest,
                                     @RequestParam("images") List<MultipartFile> images)
            throws CompanyAlreadyExistsException, CompanyException {
        long startTime = System.currentTimeMillis();
        ResponseEntity response;
        log.info("addCompany initialized" + companyRequest.toString());

        try {
            Company company = this.companyService.createCompany(companyRequest, images);
            User user = company.getUsers().get(0);
            String token = jwtUtil.generateToken(user.getEmail(), user.getAuthorities(), user.getId(), user.getName(), user.getImage(), company.getId());

            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization","Bearer " +  token );
            headers.add("access-control-expose-headers", "Authorization");
            response = ResponseEntity.ok().headers(headers).build();

        } catch (DataIntegrityViolationException ex){
            throw new CompanyAlreadyExistsException("Empresa já cadastrada com o CNPJ e/ou e-mail informado");
        }

        log.info(String.format("addCompany Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return response;
    }


    /**
     * search for Companys that contains the passed value on its name.
     *
     * @param companyName
     * @return
     */
    @GetMapping("/{companyName}")
    public ResponseEntity<List<CompanyDTO>> getCompanyByName(@PathVariable String companyName) {
        ResponseEntity response;
        long startTime = System.currentTimeMillis();
        log.info("getCompanyByName Inicializado!!");
        try {
            List<Company> companyList = companyService.findCompanyByName(companyName);
            List<CompanyDTO> res = new ArrayList<>();
            for (Company company : companyList) {
                AddressDTO address = new AddressDTO();
                address.setCity(company.getAddress().getCity());
                address.setStreet(company.getAddress().getStreet());
                address.setNumber(company.getAddress().getNumber());
                address.setDistrict(company.getAddress().getDistrict());
                res.add(CompanyDTO.builder()
                        .nomeFantasia(company.getNomeFantasia())
                        .id(company.getId())
                        .address(address)
                        .cnpj(company.getCnpj())
                        .freightValue(company.getFreightValue())
                        .build());
            }
            response = new ResponseEntity<>(res, HttpStatus.OK);
        } catch (CompanyException ex) {
            response = new ResponseEntity<>(new ProcessResult(ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        log.info(String.format("getCompanyByName Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return response;
    }

    @PostMapping("/update-freight")
    public ResponseEntity<String> updateFreightValue(@Valid @RequestBody FreightValueRequest freightValueRequest) {
        ResponseEntity response;
        long startTime = System.currentTimeMillis();
        log.info("M updateFreightValue initialized");
        try {
               response = companyService.saveFreightValue(freightValueRequest);
        } catch (Exception ex){
            response = new ResponseEntity<>("Error tying to update freight!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        log.info(String.format("M updateFreightValue finalized in %s ms!", System.currentTimeMillis() - startTime));
        return response;
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping("{id}/save-users")
    public ResponseEntity saveUser(@PathVariable Long id, @RequestBody List<UserRequest> list) {
        this.companyService.saveNewUser(id, list);
        return ResponseEntity.ok().build();
    }
}
