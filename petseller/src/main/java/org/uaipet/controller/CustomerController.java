package org.uaipet.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.uaipet.dto.request.AddressRequest;
import org.uaipet.dto.CustomerDTO;
import org.uaipet.dto.request.CustomerRequest;
import org.uaipet.dto.request.ValidateCustomerRequest;
import org.uaipet.data.repository.CustomerRepository;
import org.uaipet.data.repository.ValidationTokenRepository;
import org.uaipet.service.EmailService;
import org.uaipet.service.CustomerService;
import org.uaipet.utils.InputFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import javax.validation.Valid;
import java.util.List;

import static org.uaipet.dto.utils.Builder.buildAddress;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    private static final Logger log = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private InputFilter inputFilter;

    @Autowired
    private EmailService emailService;

    @Autowired
    private ValidationTokenRepository validationTokenRepository;

    /**
     * Create or update a Customer, except for the e-mail that information can not be updated
     *
     * @param customerReq
     * @return Customer
     */
    @PostMapping
    @ApiOperation("Criar ou atualizar dados do cliente")
    public ResponseEntity save(@RequestParam("customer") CustomerRequest customerReq,
                               @RequestParam("images") List<MultipartFile> images) {
        ResponseEntity response;
        long startTime = System.currentTimeMillis();
        log.info("saveCustomerInfo Inicializado!!");

        response = this.customerService.save(customerReq, images);

        log.info(String.format("saveCustomerInfo Finalizado em %s ms!", System.currentTimeMillis() - startTime));

        return response;
    }

    @GetMapping("/{id}")
    @ApiOperation("Buscar usuário pelo id")
    public ResponseEntity<Boolean> findById(@PathVariable Long id) {
        ResponseEntity response;
        long startTime = System.currentTimeMillis();
        log.info("find customer by id initialized!!");
        response = ResponseEntity.ok(this.customerService.findById(id));
        log.info(String.format("find customer by id finalized in %s ms!", System.currentTimeMillis() - startTime));
        return response;
    }


    @PostMapping(value = "/validate")
    @ApiOperation("Valida o cliente com cadastro completo")
    public ResponseEntity<Boolean> validateCustomer(@RequestBody ValidateCustomerRequest validateCustomerRequest) {
        ResponseEntity response;
        long startTime = System.currentTimeMillis();
        log.info("validateCustomer Inicializado!!");
        response = ResponseEntity.ok(customerService.userValidation(validateCustomerRequest));
        log.info(String.format("validateCustomer Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return response;
    }


    @PutMapping( "/{id}" )
    @ApiOperation("Atualizar um cliente")
    public ResponseEntity update( @PathVariable Long id, @Valid @RequestBody CustomerDTO customerDTO ) {

        ResponseEntity response;
        long startTime = System.currentTimeMillis();
        log.info("Update customer inicializado!!");
        response = ResponseEntity.ok( this.customerService.update( id, customerDTO ) );
        log.info(String.format("validateCustomer Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return response;
    }


    @PutMapping(value = "/{id}/address")
    @ApiOperation("Atualizar endereço do usuário")
    public ResponseEntity<Boolean> registerUserAddress(@PathVariable Long id, @Valid @RequestBody List<AddressRequest> address ) {
        ResponseEntity response;
        long startTime = System.currentTimeMillis();
        log.info("Update custumer addresses initialized!!");
        response = ResponseEntity.ok(customerService.updateAddress(id, buildAddress(address)));
        log.info(String.format("registerUserAddress finalized in %s ms!", System.currentTimeMillis() - startTime));
        return response;
    }

}
