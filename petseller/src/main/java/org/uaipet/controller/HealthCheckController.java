package org.uaipet.controller;

import org.uaipet.data.repository.CustomerRepository;
import org.uaipet.utils.ProcessResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class HealthCheckController {

    private static final Logger log = LoggerFactory.getLogger(CompanyController.class);

    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping(value = "/healthCheck")
    public ResponseEntity<String> healthCheck() {
        ResponseEntity response;
        try {
            log.info("healthCheck Inicializado!!");
            response = new ResponseEntity<>("Health Check OK", HttpStatus.OK);
        } catch (Exception ex) {
            response = new ResponseEntity<>(new ProcessResult(ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }


}
