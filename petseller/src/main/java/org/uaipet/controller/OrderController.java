package org.uaipet.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.uaipet.dto.CompanyDTO;
import org.uaipet.data.model.OrderStatus;
import org.uaipet.dto.UserOrdersDTO;
import org.uaipet.dto.request.UserOrdersRequest;
import org.uaipet.projection.OnGoingOrderProjection;
import org.uaipet.data.repository.OrderRepository;
import org.uaipet.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.uaipet.service.UserService;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/orders")
public class OrderController {

    private static final Logger log = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserService userService;

    @GetMapping(value = "list/company")
    public ResponseEntity<List<UserOrdersDTO>> listOngoingOrders(@RequestBody UserOrdersRequest ordersRequest) {
            userService.requestUserIsAssociatedWithCompany(ordersRequest.getUserId(),ordersRequest.getCompanyId());
            return ResponseEntity.ok(orderService.getCompanyOrders(ordersRequest));
    }

    @PostMapping(value = "/move")
    public ResponseEntity<Boolean> moveOrderToNextStatus(@RequestBody UserOrdersRequest ordersRequest) {
        long startTime = System.currentTimeMillis();
        log.info("moveOrderToNextStatus Inicializado!!");
        userService.requestUserIsAssociatedWithCompany(ordersRequest.getUserId(),ordersRequest.getCompanyId());
        AtomicReference<ResponseEntity> response = new AtomicReference<>();
        try {
            Optional<OrderStatus> order = orderRepository.findById(ordersRequest.getOrderId());
            order.ifPresent( e ->
            {
                orderService.updateToNextStatus(e);
                response.set(new ResponseEntity<>("Status do pedido atualizado com sucesso!", HttpStatus.OK));
            });
        } catch (Exception ex) {
            log.error(ex.getMessage());
            response.set(new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR));
        }
        log.info(String.format("moveOrderToNextStatus Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return response.get();
    }

    @PostMapping(value = "/{userId}")
    public ResponseEntity<List<UserOrdersDTO>> findOrdersFromUser(@RequestBody UserOrdersRequest ordersRequest) {
        log.info("findOrders Inicializado!!");
        return ResponseEntity.ok(orderService.getUserOrders(ordersRequest));
    }


    @GetMapping(value = "/report-problem/{orderId}")
    public ResponseEntity<CompanyDTO> reportProblem(@PathVariable Long orderId) {
        log.info("reportProblem initialized!");

   return ResponseEntity.ok(orderService.getMerchantIfoToReportProblem(orderId));
}
}
