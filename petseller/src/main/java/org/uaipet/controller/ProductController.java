package org.uaipet.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.uaipet.dto.*;
import org.uaipet.dto.enums.MeasurementUnitEnum;
import org.uaipet.dto.enums.TransactionStatus;
import org.uaipet.dto.request.ProductRequest;
import org.uaipet.exception.CustomerNotFoundException;
import org.uaipet.exception.EmailNotValidatedException;
import org.uaipet.exception.ProductException;
import org.uaipet.exception.CustomerException;
import org.uaipet.data.model.*;
import org.uaipet.data.repository.ProductRepository;
import org.uaipet.data.repository.SearchIndexRepository;
import org.uaipet.data.repository.AddressRepository;
import org.uaipet.data.repository.CustomerRepository;
import org.uaipet.service.ProductService;
import org.uaipet.service.CustomerService;
import org.uaipet.service.UserService;
import org.uaipet.utils.InputFilter;
import org.uaipet.utils.RegularExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
public class ProductController {
    private static final Logger log = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private InputFilter inputFilter;

    @Autowired
    private SearchIndexRepository searchIndexRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private UserService userService;

    /**
     * Add 1 product related to a specific merchant
     */
    @PutMapping(value = "/product")
    public ResponseEntity<Product> addProduct(@RequestParam("product") ProductRequest product,
                                              @RequestParam("images") List<MultipartFile> images) {
        ResponseEntity response;
        long startTime = System.currentTimeMillis();
        log.info(String.format("addProduct Inicializado - %s", product.toString()));
        try {
            if (validateObjectInfo(product)) {
                Product addedProduct = productService.createProduct(product, images);
                ProductDTO productDTO = ProductDTO.builder()
                        .id(addedProduct.getId())
                        .name(addedProduct.getName())
                        .image(addedProduct.getImage())
                        .build();
                response = new ResponseEntity<>(productDTO, HttpStatus.OK);
            } else {
                response = new ResponseEntity<>("Dados Incorretos", HttpStatus.UNAUTHORIZED);
            }
        } catch (ProductException ex) {
            response = new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        log.info(String.format("addProduct Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return response;
    }


    /**
     * Show all products from a especific merchant
     *
     * @param merchantId
     * @return
     */
    @GetMapping(value = "/products/{merchantId}")
    public ResponseEntity<ProductDTO> getProductsByMerchant(@PathVariable Long merchantId) {
        ResponseEntity response;
        long startTime = System.currentTimeMillis();
        log.info("getProductsByMerchant Inicializado!!");
        try {
            List<Product> pList = productService.findProductByMerchant(merchantId);
            List<ProductDTO> resList = new ArrayList<>();
            for (Product prod : pList) {
                resList.add(ProductDTO.builder().id(prod.getId())
                        .name(prod.getName())
                        .productCategory(prod.getProductCategory())
                        .price(prod.getPrice())
                        .manufacturer(prod.getManufacturer())

                        .measurementUnitEnum(MeasurementUnitEnum.valueOf(prod.getMeasurementUnit().getName()))


                        .measurementUnitValue(prod.getMeasurementUnitValue())
                        .image(prod.getImage())
                        .company(CompanyDTO.builder()
                                .nomeFantasia(prod.getCompany().getNomeFantasia())
                                .freightValue(prod.getCompany().getFreightValue())
                                .build())
                        .build());
            }
            response = new ResponseEntity<>(resList, HttpStatus.OK);
        } catch (ProductException ex) {
            response = new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        log.info(String.format("getProductsByMerchant Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return response;
    }

    @PostMapping(value = "/find/product")
    public ResponseEntity<List<ProductDTO>> getProducts(@RequestBody ProductSearchDTO searchRequest) {
        ResponseEntity response;
        long startTime = System.currentTimeMillis();
        log.info("getProducts Initialized!!");
        try {
           return ResponseEntity.ok(productService.searchProducts(searchRequest));
        } catch (Exception ex) {
            response = new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        log.info(String.format("getProducts Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return response;
    }


    /**
     * Delete product based on the given id
     *
     * @param prodId product id
     * @return
     */
    @DeleteMapping(value = "/product")
    public ResponseEntity<String> deleteProduct(@RequestParam Long prodId) {
        ResponseEntity response;
        long startTime = System.currentTimeMillis();
        log.info("deleteProduct Inicializado!!");
        try {
            productRepository.deleteById(prodId);
            response = new ResponseEntity<>("Produto deletado com sucesso", HttpStatus.OK);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            response = new ResponseEntity<>("Erro ao deletar produto", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        log.info(String.format("deleteProduct Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return response;
    }

    /**
     *
     * @param prodId
     * @param stockCount
     * @param merchantId
     * @return
     */
    @PostMapping(value = "/update-stock")
    public ResponseEntity<String> updateStockCount(@RequestParam Long prodId, @RequestParam Long stockCount,
                                                   @RequestParam Long merchantId) {
        ResponseEntity response;
        long startTime = System.currentTimeMillis();
        log.info("updateStockCount Inicializado!!");
        try {
            response = ResponseEntity.ok(productService.updateStockCount(prodId,stockCount,merchantId));
        }
        catch (ProductException ex) {
            log.error(ex.getMessage());
            response = new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        catch (Exception ex) {
            log.error(ex.getMessage());
            response = new ResponseEntity<>("Erro ao deletar produto", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        log.info(String.format("deleteProduct Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return response;
    }



    @PostMapping(value = "/products/purchase")
    public ResponseEntity<String> buyProducts(@RequestBody PaymentRequestDTO request) {
        ResponseEntity response;
        long startTime = System.currentTimeMillis();
        log.info("buyProducts Inicializado!!");
        TransactionStatus tranResp;
        try {
            if (validateRequestInfo(request)) {
                tranResp = productService.buyProducts(request);
                if (tranResp.equals(TransactionStatus.SUCCESS)) {
                    response = new ResponseEntity<>(true, HttpStatus.OK);
                } else {
                    response = new ResponseEntity<>(tranResp, HttpStatus.INTERNAL_SERVER_ERROR);
                }
            } else {
                String msg = String.format("Dados incorretos no request %s", request.toString());
                log.error(msg);
                response = new ResponseEntity<>("Dados incorretos", HttpStatus.NOT_ACCEPTABLE);
            }
        } catch (ProductException ex) {
            response = new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        log.info(String.format("buyProducts Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return response;
    }

    private boolean validateObjectInfo(ProductRequest prod) {
        boolean inputProdName = inputFilter.validateInput(prod.getName(), RegularExpression.VALIDATE_WORDS_OR_NUMBERS.value);
        boolean inputCompanyId = inputFilter.validateInput(prod.getCompanyId().toString(), RegularExpression.VALIDATE_ONLY_NUMBER.value);
        boolean inputManufacturer = inputFilter.validateInput(prod.getManufacturer(), RegularExpression.VALIDATE_WORDS_OR_NUMBERS.value);
        boolean inputStockCount = inputFilter.validateInput(prod.getStockCount().toString(), RegularExpression.VALIDATE_ONLY_NUMBER.value);
        if (!inputProdName || !inputManufacturer || !inputStockCount || !inputCompanyId) {
            log.warn("validateObjectInfo: invalid Object");
            return false;
        }

        return true;
    }

    private boolean validateRequestInfo(PaymentRequestDTO request) {
        userService.requestUserIdEqualsTokenUserId(request.getUserId());
        Optional<Customer> customer = customerRepository.findCustomerByUserId(request.getUserId());
        if (customer.isPresent()) {
            if (!customer.get().getValid()) throw new EmailNotValidatedException();
            assert request.getShipAddressId() != null;
            assert request.getPList() != null;
            for (ProductQuantityDTO item : request.getPList()) {
                boolean inputProdId = inputFilter.validateInput(item.getProductId().toString(), RegularExpression.VALIDATE_ONLY_NUMBER.value);
                boolean inputProdQtt = inputFilter.validateInput(item.getQuantity().toString(), RegularExpression.VALIDATE_ONLY_NUMBER.value);
                if (!inputProdId || !inputProdQtt) {
                    return false;
                }
            }
            return true;
        }
        throw new CustomerNotFoundException();
    }


}
