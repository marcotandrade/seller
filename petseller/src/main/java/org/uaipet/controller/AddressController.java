package org.uaipet.controller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.uaipet.client.GeoCodingClient;
import org.uaipet.dto.UserAddressDTO;
import org.uaipet.generated.GeoCodingResponse;
import org.uaipet.data.model.Customer;
import org.uaipet.data.model.DeliveryArea;
import org.uaipet.data.model.Address;
import org.uaipet.data.repository.AddressRepository;
import org.uaipet.data.repository.CustomerRepository;
import org.uaipet.service.AddressService;
import org.uaipet.service.SearchIndexService;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@Transactional
public class AddressController {

    private static final Logger log = LoggerFactory.getLogger(AddressController.class);

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private SearchIndexService searchIndexService;

    @Autowired
    private GeoCodingClient geoCodingClient;

    @Autowired
    private AddressService addressService;

    @Value("${cloud.google.geo-coding-api.key}")
    private String googleApiKey;

    @PostMapping(value = "/userAddress")
    public ResponseEntity<DeliveryArea> addUserAddress(@Valid @RequestBody UserAddressDTO userAddress) {
        ResponseEntity response;
        long startTime = System.currentTimeMillis();
        log.info("addUserAddress Inicializado!!");
        try {
            if (validateInput(userAddress)) {
                //add latitude and longitude to the address
                GeoCodingResponse latlon = geoCodingClient.fetchGeoLocationInfo(getAddressOnApiFormat(userAddress), googleApiKey);
                userAddress.setLatitude(latlon.getResults().get(0).getGeometry().getLocation().getLat().toString());
                userAddress.setLongitude(latlon.getResults().get(0).getGeometry().getLocation().getLng().toString());
                addressRepository.updateAddressesNonDefault(userAddress.getCustomerId());
                userAddress.setDefaultAddress(true);
                Address address = addressRepository.save(getEntityFromDto(userAddress));
                searchIndexService.saveSearchIndex(address, null, userAddress.getCustomerId());
                response = new ResponseEntity<>("Endereço cadastrado com sucesso!", HttpStatus.OK);
            } else {
                response = new ResponseEntity<>("Dados inválidos", HttpStatus.NOT_ACCEPTABLE);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
            response = new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        log.info(String.format("addUserAddress Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return response;
    }

    @PostMapping(value = "/user-address/update")
    public ResponseEntity<Boolean> updateUserAddress(@Valid @RequestBody UserAddressDTO userAddress) {
        ResponseEntity response;
        long startTime = System.currentTimeMillis();
        log.info("updateUserAddress initialized");
        try {
            if (validateUpdateInput(userAddress)) {
                response = ResponseEntity.ok(addressService.setDefaultAddress(userAddress));
            } else {
                response = new ResponseEntity<>("Dados inválidos", HttpStatus.NOT_ACCEPTABLE);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
            response = new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        log.info(String.format("updateUserAddress Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return response;
    }

    private String getAddressOnApiFormat(UserAddressDTO userAddress) {
        return userAddress.getStreet().concat("+").concat(userAddress.getNumber().toString())
                .concat("+").concat(userAddress.getCity());
    }

    private boolean validateInput(UserAddressDTO userAddress) {
        return  StringUtils.isNotBlank(userAddress.getStreet()) && StringUtils.isNotBlank(userAddress.getNumber().toString()) &&
                StringUtils.isNotBlank(userAddress.getDistrict()) && StringUtils.isNotBlank(userAddress.getCity());
    }

    private boolean validateUpdateInput(UserAddressDTO userAddress) {
        return  StringUtils.isNotBlank(userAddress.getAddressId().toString()) && StringUtils.isNotBlank(userAddress.getCustomerId().toString());
    }

    private Address getEntityFromDto(UserAddressDTO userAddress) {
        Optional<Customer> customer = customerRepository.findById(userAddress.getCustomerId());
        return customer.map(value -> Address.builder()
                .city(userAddress.getCity())
                .defaultAddress(true)
                .complement(userAddress.getComplement())
                .district(userAddress.getDistrict())
                //.latitude(userAddress.getLatitude())
                //.latitude(userAddress.getLatitude())
                .number(userAddress.getNumber())
                .postalCode(userAddress.getPostalCode())
                .state(userAddress.getState())
                .street(userAddress.getStreet())
                .build()).orElse(null);
    }
}
