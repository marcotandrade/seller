package org.uaipet.controller;

import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.uaipet.dto.EmailDTO;
import org.uaipet.security.JWTUtil;
import org.uaipet.security.UserSS;
import org.uaipet.service.AuthService;
import org.uaipet.service.UserService;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "/auth")
public class AuthController {

    private static final Logger log = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    private AuthService service;

    @PostMapping("/refresh_token")
    @ApiOperation("Atualizar o token")
    public ResponseEntity<Void> refreshToken(HttpServletResponse response) {
        UserSS user = UserService.authenticated();
        String token =
                jwtUtil.generateToken(
                        user.getUsername(),
                        user.getAuthorities(),
                        user.getUserId(),
                        user.getName(),
                        user.getImage(),
                        user.getCompanyId()
                );

        response.addHeader("Authorization", "Bearer " + token);
        response.addHeader("access-control-expose-headers", "Authorization");
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/forgot")
    @ApiOperation("Informar e-mail do usuàrio para geracao de nova senha")
    public ResponseEntity<Void> forgot(@Valid @RequestBody EmailDTO dto) {
        long startTime = System.currentTimeMillis();
        log.info("passwordForgotten Inicializado in: " + startTime);
        service.sendNewPassword(dto.getEmail());
        log.info(String.format("passwordForgotten Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return ResponseEntity.noContent().build();
    }
}
