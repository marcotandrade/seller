package org.uaipet.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.uaipet.data.model.ProductCategory;
import org.uaipet.data.repository.ProductCategoryRepository;

@RestController
public class ProductCategoryController {

    private static final Logger log = LoggerFactory.getLogger(ProductCategoryController.class);

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @GetMapping(value = "/findAll/ProductCategory")
    public ResponseEntity<Iterable<ProductCategory>> getAllCategories(){
        long startTime = System.currentTimeMillis();
        log.info("getAllCategories initialized!!");
        Iterable<ProductCategory> productCategories = productCategoryRepository.findAll();
        log.info(String.format("getAllCategories Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return ResponseEntity.ok(productCategories);

    }



}
