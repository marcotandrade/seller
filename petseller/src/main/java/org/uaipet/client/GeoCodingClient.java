package org.uaipet.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.uaipet.generated.GeoCodingResponse;

@FeignClient(value = "GeoCoding", url = "${cloud.google.geo-coding-api.host}")
public interface GeoCodingClient {

    @RequestMapping(method = RequestMethod.GET, path = "${cloud.google.geo-coding-api.url}")
    GeoCodingResponse fetchGeoLocationInfo(@PathVariable("fullAddress") String address, @PathVariable("apiKey") String apiKey);

}
