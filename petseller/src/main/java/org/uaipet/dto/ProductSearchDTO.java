package org.uaipet.dto;

import java.util.Objects;

public class ProductSearchDTO {

    private String name;
    private Long animalCategory;
    private Long productCategory;
    private String sortedBy;
    private String direction;
    private Integer page;
    private Long userId;
    private Long addressId;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getAnimalCategory() {
        return animalCategory;
    }

    public void setAnimalCategory(Long animalCategory) {
        this.animalCategory = animalCategory;
    }

    public Long getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(Long productCategory) {
        this.productCategory = productCategory;
    }

    public String getSortedBy() {
        return sortedBy;
    }

    public void setSortedBy(String sortedBy) {
        this.sortedBy = sortedBy;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductSearchDTO that = (ProductSearchDTO) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(animalCategory, that.animalCategory) &&
                Objects.equals(productCategory, that.productCategory) &&
                Objects.equals(sortedBy, that.sortedBy) &&
                Objects.equals(direction, that.direction) &&
                Objects.equals(page, that.page) &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(addressId, that.addressId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, animalCategory, productCategory, sortedBy, direction, page, userId, addressId);
    }
}
