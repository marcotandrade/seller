package org.uaipet.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
public class EmailDTO {

    @NotEmpty(message="Preenchimento obrigatório")
    @Email(message="Email inválido")
    private String email;
}
