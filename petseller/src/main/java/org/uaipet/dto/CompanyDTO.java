package org.uaipet.dto;


import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;

@Data
@ToString
@Builder
public class CompanyDTO {
    private Long id;
    private String razaoSocial;
    private String nomeFantasia;
    private String cnpj;
    private AddressDTO address;
    private Double fee;
    private Double monthlyFee;
    private String email;
    private Double freightValue;
    private String phoneNumber;
    private ArrayList<String> images;

}
