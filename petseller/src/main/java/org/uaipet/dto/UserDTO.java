package org.uaipet.dto;

import lombok.Data;

import java.util.ArrayList;

@Data
public class UserDTO {

    private Long id;
    private String name;
    private ArrayList<String> image;
    private String email;
    private String password;
}
