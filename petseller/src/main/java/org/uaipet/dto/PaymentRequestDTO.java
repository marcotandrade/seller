package org.uaipet.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Data
@ToString
public class PaymentRequestDTO implements Serializable {

    @JsonProperty("pList")
    private List<ProductQuantityDTO> pList;
    private Long shipAddressId;
    private Long userId;

}
