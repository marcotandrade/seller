package org.uaipet.dto;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.uaipet.data.model.ProductCategory;
import org.uaipet.dto.enums.MeasurementUnitEnum;

import java.io.Serializable;
import java.util.ArrayList;

@Data
@Builder
@EqualsAndHashCode
public class ProductDTO implements Serializable {

    private Long id;
    private String name;
    private ProductCategory productCategory;
    private Double price;
    private CompanyDTO company;
    private String manufacturer;
    private MeasurementUnitEnum measurementUnitEnum;
    private String measurementUnitValue;
    private ArrayList<String> image;



}
