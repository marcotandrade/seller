package org.uaipet.dto;

import org.uaipet.data.model.Product;

import java.util.Date;
import java.util.List;

public class TranDTO {

    private Long orderId;
    private String transactionCode;
    private Double amount;
    private Date date;
    private String msisdn;
    private String status;
    private List<Product> productList;

    public TranDTO() {
    }

    public TranDTO(Long orderId, String transactionCode, Double amount, Date date, String msisdn, String status, List<Product> productList) {
        this.orderId = orderId;
        this.transactionCode = transactionCode;
        this.amount = amount;
        this.date = date;
        this.msisdn = msisdn;
        this.status = status;
        this.productList = productList;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }
}
