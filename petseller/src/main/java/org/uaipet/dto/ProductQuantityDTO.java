package org.uaipet.dto;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class ProductQuantityDTO implements Serializable {

    private Long productId;
    private Integer quantity;


}
