package org.uaipet.dto.enums;

public enum TransactionStatus {

    SUCCESS("success"),
    PENDING("pending"),
    CHARGE_BACK("charge-back"),
    CANCELLED("cancelled"),
    WAIT_CONFIRMATION("waiting-confirmation"),
    FAIL("fail");

    public String name;

    public String getName() {
        return name;
    }

    TransactionStatus(String name) {
        this.name = name;
    }
}
