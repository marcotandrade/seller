package org.uaipet.dto.enums;

public enum CityEnum {

    UBERLANDIA("Uberlândia");

    public String name;

    public String getName() {
        return name;
    }

    CityEnum(String name) {
        this.name = name;
    }
}
