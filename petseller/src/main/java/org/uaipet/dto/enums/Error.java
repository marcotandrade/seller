package org.uaipet.dto.enums;

public enum Error {

    INVALID_TOKEN("Invalid Token","50001");


    public String message;
    public String errorCode;

    Error(String message, String errorCode) {
        this.message = message;
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public String getErrorCode() {
        return errorCode;
    }
}
