package org.uaipet.dto.enums;

public enum MeasurementUnitEnum {

    Quilograma("Quilograma"),
    Grama("Grama"),
    Miligrama("Miligrama"),
    Tamanho("Tamanho"),
    Litros("Litros"),
    Outros("Outros");

    private final String name;

    MeasurementUnitEnum(String name) {
        this.name = name;
    }
}
