package org.uaipet.dto.enums;

public enum OrderStatusEnum {
    WAITING_CONFIRMATION,
    PREPARING_PRODUCT,
    SENT_TO_DELIVERY,
    FINISHED
}
