package org.uaipet.dto.enums;

public enum AnimalAgeEnum {

    newborn("newborn"),
    puppy("puppy"),
    adult("adult"),
    elderly("elderly");

    public String name;


    AnimalAgeEnum(String name) {
    }

}
