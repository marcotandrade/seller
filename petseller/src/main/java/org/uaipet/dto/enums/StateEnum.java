package org.uaipet.dto.enums;

public enum StateEnum {

    MG("MG");

    public String name;

    public String getName() {
        return name;
    }

    StateEnum(String name) {
        this.name = name;
    }
}
