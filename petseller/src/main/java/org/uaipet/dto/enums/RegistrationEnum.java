package org.uaipet.dto.enums;

public enum RegistrationEnum {
    COMPLETE("complete"),
    INCOMPLETE("Incomplete");

    public String name;

    public String getName() {
        return name;
    }

    RegistrationEnum(String name) {
        this.name = name;
    }
}
