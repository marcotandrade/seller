package org.uaipet.dto;

import lombok.Builder;
import lombok.Data;
import org.uaipet.dto.enums.StateEnum;

@Data
@Builder
public class UserAddressDTO {

    private Long addressId;

    private StateEnum state;

    private String street;

    private Long number;

    private String district;

    private String complement;

    private Long postalCode;

    private String city;

    private String latitude;

    private String longitude;

    private Boolean defaultAddress;

    private Long customerId;
}
