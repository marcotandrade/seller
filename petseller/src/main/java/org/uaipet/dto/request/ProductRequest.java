package org.uaipet.dto.request;


import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class ProductRequest {

    private String name;
    private Long productCategory;
    private Long animalCategory;
    private Double price;
    private Long companyId;
    private Long userId;
    private String manufacturer;
    private Long stockCount;
    private Long measurementUnit;
    private Long animalAge;
    private String measurementUnitValue;
    private String description;

}
