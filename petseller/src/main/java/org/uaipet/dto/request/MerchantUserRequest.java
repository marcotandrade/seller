package org.uaipet.dto.request;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;

public class MerchantUserRequest {

    private String fullName;

    private String email;

    private String password;

    private Long merchant;

    private Boolean masterUser;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getMerchant() {
        return merchant;
    }

    public void setMerchant(Long merchant) {
        this.merchant = merchant;
    }

    public Boolean getMasterUser() {
        return masterUser;
    }

    public void setMasterUser(Boolean masterUser) {
        this.masterUser = masterUser;
    }

    @Override
    public String toString() {
        return "MerchantUserRequest{" +
                "fullName='" + fullName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", merchant=" + merchant +
                ", masterUser=" + masterUser +
                '}';
    }
}
