package org.uaipet.dto.request;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class DefaultAddressRequest {

    private Long customerId;
    private Long addressId;

}
