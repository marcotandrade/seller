package org.uaipet.dto.request;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class UserOrdersRequest {
    private Long userId;
    private Long companyId;
    private Integer page;
    private Long orderId;
}
