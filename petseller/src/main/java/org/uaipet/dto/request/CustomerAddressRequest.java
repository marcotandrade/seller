package org.uaipet.dto.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerAddressRequest {

    private AddressRequest addressRequest;
    private Long customerId;

}
