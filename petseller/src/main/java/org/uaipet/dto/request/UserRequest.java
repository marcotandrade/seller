package org.uaipet.dto.request;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@ToString
public class UserRequest {

    private String name;

    private String email;

    private String password;

    private ArrayList<String> image;

    private List<Integer> profiles;
}
