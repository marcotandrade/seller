package org.uaipet.dto.request;

import lombok.Builder;
import lombok.Data;
import org.uaipet.dto.enums.CityEnum;
import org.uaipet.dto.enums.StateEnum;

@Data
@Builder
public class CustomerRequest {

    private String expoToken;
    private String fullName;
    private String msisdn;
    private String email;
    private String password;
    private StateEnum state;
    private String street;
    private Long addressNumber;
    private String district;
    private String addressComplement;
    private Long postalCode;
    private CityEnum city;
    private Boolean defaultAddress;

}
