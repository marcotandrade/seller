package org.uaipet.dto.request;

import lombok.Data;

@Data
public class ValidateCustomerRequest {

    private Long userId;
    private String email;
    private String token;

}
