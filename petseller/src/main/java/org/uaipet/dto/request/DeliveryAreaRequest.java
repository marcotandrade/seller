package org.uaipet.dto.request;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.uaipet.generated.Location;

import java.util.ArrayList;

@Data
@Builder
@ToString
public class DeliveryAreaRequest {

    private Long userId;
    private Long companyId;
    private ArrayList<Location> locations;

}
