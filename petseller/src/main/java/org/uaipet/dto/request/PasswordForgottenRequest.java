package org.uaipet.dto.request;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PasswordForgottenRequest {

    private String email;
    private Long id;

}
