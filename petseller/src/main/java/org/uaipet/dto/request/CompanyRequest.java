package org.uaipet.dto.request;


import lombok.*;
import org.uaipet.dto.enums.CityEnum;
import org.uaipet.dto.enums.StateEnum;

@Data
@Builder
@ToString
public class CompanyRequest {
    private String razaoSocial;
    private String nomeFantasia;
    private String cnpj;
    private Double freightValue;
    private String phoneNumber;
    private StateEnum state;
    private String street;
    private Long addressNumber;
    private String district;
    private String addressComplement;
    private Long postalCode;
    private CityEnum city;
    private String userEmail;
    private String userPassword;
}
