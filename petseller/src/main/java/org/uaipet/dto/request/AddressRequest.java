package org.uaipet.dto.request;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.uaipet.dto.enums.CityEnum;
import org.uaipet.dto.enums.StateEnum;

@Data
@Builder
@ToString
public class AddressRequest {
    private Long id;
    private StateEnum state;
    private String street;
    private Long addressNumber;
    private String district;
    private String addressComplement;
    private Long postalCode;
    private CityEnum city;
    private Double latitude;
    private Double longitude;
    private Boolean defaultAddress;
}
