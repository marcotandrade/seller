package org.uaipet.dto.request;

import lombok.Data;

@Data
public class FreightValueRequest {

    private Long companyId;
    private Double freightValue;
    private String email;

}
