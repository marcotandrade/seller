package org.uaipet.dto.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.uaipet.data.model.Address;
import org.uaipet.data.model.Company;
import org.uaipet.data.model.Customer;
import org.uaipet.data.model.User;
import org.uaipet.dto.CustomerDTO;
import org.uaipet.dto.UserDTO;
import org.uaipet.dto.request.AddressRequest;
import org.uaipet.dto.request.CompanyRequest;
import org.uaipet.dto.request.CustomerRequest;
import org.uaipet.dto.request.UserRequest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.uaipet.data.enums.Profile.*;
import static org.uaipet.data.enums.UserStatus.ACTIVE;
import static org.uaipet.data.enums.UserType.PESSOAFISICA;
import static org.uaipet.data.enums.UserType.PESSOAJURIDICA;

public class Builder {


    @Autowired


    public static Company builderCompany(CompanyRequest request){
        return Company.builder()
                .razaoSocial(request.getRazaoSocial())
                .nomeFantasia(request.getNomeFantasia())
                .cnpj(request.getCnpj())
                .phoneNumber(request.getPhoneNumber())
                .freightValue(request.getFreightValue())
                .users(Collections.singletonList(builderUser(request, true)))
                .address(buildAddress(request))
                .build();
    }

    public static Customer builderCustomer(CustomerDTO dto) {
        return Customer.builder()
                .id(dto.getId())
                .expoToken(dto.getExpoToken())
                .fullName(dto.getFullName())
                .msisdn(dto.getMsisdn())
                .user(builderUser(dto.getUser(), false))
                .address(dto.getAddress().stream().map(Builder::buildAddress).collect(Collectors.toList()))
                .build();
    }


    public static Customer builderCustomer(CustomerRequest dto) {
        List<Address> addresses = new ArrayList<>();
        addresses.add(buildAddress(dto));

        return Customer.builder()
                .expoToken(dto.getExpoToken())
                .fullName(dto.getFullName())
                .msisdn(dto.getMsisdn())
                .user(builderUser(dto, false))
                .address(addresses)
                .build();
    }


    public static User builderUser(UserDTO dto, boolean isCompany) {

        if(Objects.isNull(dto)) return null;

        return User.builder()
                .id(dto.getId())
                .name(dto.getName())
                .email(dto.getEmail())
                .password(dto.getPassword())
                .image(dto.getImage())
                .type(isCompany ? PESSOAJURIDICA.getCod() : PESSOAFISICA.getCod())
                .profiles(Collections.singleton(isCompany ? ADMIN.getCod(): CLIENT.getCod()))
                .status(ACTIVE.getCod())
                .build();
    }

    public static User builderUser(UserRequest request, Company company, BCryptPasswordEncoder encoder) {

        if(Objects.isNull(request)) return null;

        return User.builder()
                .name(request.getName())
                .email(request.getEmail())
                .password(encoder.encode(request.getPassword()))
                .image(request.getImage())
                .type(PESSOAFISICA.getCod())
                .profiles(request.getProfiles().stream().map(p -> toEnum(p).getCod()).collect(Collectors.toSet()))
                .status(ACTIVE.getCod())
                .company(company)
                .build();
    }

    public static List<Address> buildAddress(List<AddressRequest> list) {
        return list.stream().map(Builder::buildAddress).collect(Collectors.toList());
    }

    public static User builderUser(CustomerRequest dto, boolean isCompany) {
        return User.builder()
                .name(dto.getFullName())
                .email(dto.getEmail())
                .password(dto.getPassword())
                .type(isCompany ? PESSOAJURIDICA.getCod() : PESSOAFISICA.getCod())
                .profiles(Collections.singleton(isCompany ? ADMIN.getCod(): CLIENT.getCod()))
                .status(ACTIVE.getCod())
                .build();
    }

    public static User builderUser(CompanyRequest dto, boolean isCompany) {
        return User.builder()
                .name(dto.getNomeFantasia())
                .email(dto.getUserEmail())
                .password(dto.getUserPassword())
                .type(isCompany ? PESSOAJURIDICA.getCod() : PESSOAFISICA.getCod())
                .profiles(Collections.singleton(isCompany ? ADMIN.getCod(): CLIENT.getCod()))
                .status(ACTIVE.getCod())
                .build();
    }

    public static Address buildAddress(AddressRequest addressRequest) {
        return Address.builder()
                .id(addressRequest.getId())
                .latitude(addressRequest.getLatitude())
                .longitude(addressRequest.getLongitude())
                .number(addressRequest.getAddressNumber())
                .complement(addressRequest.getAddressComplement())
                .city(addressRequest.getCity().getName())
                .district(addressRequest.getDistrict())
                .postalCode(addressRequest.getPostalCode())
                .street(addressRequest.getStreet())
                .state(addressRequest.getState())
                .defaultAddress(Objects.isNull(addressRequest.getDefaultAddress()) ? true : addressRequest.getDefaultAddress())
                .build();
    }

    public static Address buildAddress(CompanyRequest companyRequest) {
        return Address.builder()
                .number(companyRequest.getAddressNumber())
                .complement(companyRequest.getAddressComplement())
                .city(companyRequest.getCity().getName())
                .district(companyRequest.getDistrict())
                .postalCode(companyRequest.getPostalCode())
                .street(companyRequest.getStreet())
                .state(companyRequest.getState())
                .defaultAddress(true)
                .build();
    }
    public static Address buildAddress(CustomerRequest customerRequest) {
        return Address.builder()
                .number(customerRequest.getAddressNumber())
                .complement(customerRequest.getAddressComplement())
                .city(customerRequest.getCity().getName())
                .district(customerRequest.getDistrict())
                .postalCode(customerRequest.getPostalCode())
                .street(customerRequest.getStreet())
                .state(customerRequest.getState())
                .defaultAddress(true)
                .build();
    }
}
