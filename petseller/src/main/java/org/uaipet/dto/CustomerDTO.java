package org.uaipet.dto;


import lombok.Builder;
import lombok.Data;
import org.uaipet.dto.request.AddressRequest;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
public class CustomerDTO {

    private Long id;
    private String expoToken;
    private String fullName;
    private String msisdn;
    private ArrayList<String> photoUrl;
    private UserDTO user;
    private List<AddressRequest> address;

}
