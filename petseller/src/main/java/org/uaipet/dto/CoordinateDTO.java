package org.uaipet.dto;

public class CoordinateDTO {

    private Double longitude;
    private Double latitude;

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public CoordinateDTO(Double longitude, Double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }
}
