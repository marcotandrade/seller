package org.uaipet.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UserOrdersDTO {

    private String date;
    private Long orderNumber;
    private String orderStatus;
    private String companyName;
    private String amount;
    private List<UserOrderProductsDTO> userOrderProducts;
    private String street;
    private Integer addressNumber;
    private String complement;


}
