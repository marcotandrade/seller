package org.uaipet.dto;

import org.springframework.web.multipart.MultipartFile;

public class ProductImages {

    private String name;
    private MultipartFile multipartFile;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }

    public ProductImages() {
    }
}
