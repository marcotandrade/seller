package org.uaipet.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserOrderProductsDTO {

    private String productName;
    private Integer productQtt;
    private String productPrice;

}
