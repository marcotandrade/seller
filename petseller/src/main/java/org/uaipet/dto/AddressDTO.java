package org.uaipet.dto;


import lombok.Data;
import org.uaipet.dto.enums.StateEnum;
import org.uaipet.data.model.Address;

@Data
public class AddressDTO {

    private Long id;
    private StateEnum state;
    private String street;
    private Long number;
    private String district;
    private String complement;
    private Long postalCode;
    private String city;
    private Double latitude;
    private Double longitude;


    public AddressDTO(Address address) {
        this.street = address.getStreet();
        this.number = address.getNumber();
        this.district = address.getDistrict();
        this.complement = address.getComplement();
        this.postalCode = address.getPostalCode();
        this.city = address.getCity();
        this.latitude = address.getLatitude();
        this.longitude = address.getLongitude();
        this.id = address.getId();
        this.state = address.getState();
    }

    public AddressDTO() {
    }
}
