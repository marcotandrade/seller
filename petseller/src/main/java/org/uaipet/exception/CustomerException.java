package org.uaipet.exception;

import org.uaipet.dto.enums.Error;

public class CustomerException extends DefaultException {

    @Override
    public String getMessage() {
        return super.getMessage();
    }

    public CustomerException(String message) {
        super(message);
    }

    public CustomerException(Error error) {
        super(error);
    }
}
