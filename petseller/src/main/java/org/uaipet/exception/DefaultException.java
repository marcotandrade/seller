package org.uaipet.exception;

import org.uaipet.dto.enums.Error;

public class DefaultException extends RuntimeException{
    private Error error;

    public DefaultException(String message) {
        super(message);
    }

    public DefaultException(Error error) {
        this.error = error;
    }

    public DefaultException(String message, Error error) {
        super(message);
        this.error = error;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }
}
