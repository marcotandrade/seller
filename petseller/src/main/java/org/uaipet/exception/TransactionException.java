package org.uaipet.exception;

public class TransactionException extends Exception {

    @Override
    public String getMessage() {
        return super.getMessage();
    }

    public TransactionException() {
        super();
    }

    public TransactionException(String message) {
        super(message);
    }

    public TransactionException(Exception ex) {
        super(ex);
    }
}
