package org.uaipet.exception;

public class InvalidAddressException extends DefaultException {

    public InvalidAddressException(String message) {
        super(message);
    }


    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
