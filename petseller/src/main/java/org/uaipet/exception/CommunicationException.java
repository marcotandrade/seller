package org.uaipet.exception;

public class CommunicationException extends Exception {


    @Override
    public String getMessage() {
        return super.getMessage();
    }

    public CommunicationException() {
        super();
    }

    public CommunicationException(String message) {
        super(message);
    }

    public CommunicationException(String message, Exception ex) {
        super(message, ex);
    }


}
