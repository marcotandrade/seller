package org.uaipet.exception;

public class UserNotAssociatedWithCompanyException extends RuntimeException {

    public UserNotAssociatedWithCompanyException() {
        super();
    }

    public UserNotAssociatedWithCompanyException(String s) {
        super(s);
    }
}
