package org.uaipet.exception;

public class KmsException extends Exception {

    @Override
    public String getMessage() {
        return super.getMessage();
    }

    public KmsException() {
        super();
    }

    public KmsException(String message) {
        super(message);
    }

    public KmsException(String message, Throwable ex) {
        super(message, ex);
    }

}
