package org.uaipet.exception;

public class ProductException extends Exception {

    @Override
    public String getMessage() {
        return super.getMessage();
    }

    public ProductException() {
        super();
    }

    public ProductException(String message) {
        super(message);
    }

    public ProductException(Exception ex, String message) {
        super(message, ex);
    }
}
