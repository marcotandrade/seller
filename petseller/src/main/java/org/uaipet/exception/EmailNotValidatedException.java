package org.uaipet.exception;

public class EmailNotValidatedException extends RuntimeException {

    public EmailNotValidatedException() {
        super();
    }

    public EmailNotValidatedException(String s) {
        super(s);
    }
}
