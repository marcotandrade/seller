package org.uaipet.exception;

public class CompanyNotFoundException extends RuntimeException {


    @Override
    public String getMessage() {
        return super.getMessage();
    }

    public CompanyNotFoundException() {
        super();
    }

    public CompanyNotFoundException(String message) {
        super(message);
    }


}
