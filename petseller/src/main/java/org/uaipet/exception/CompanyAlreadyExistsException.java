package org.uaipet.exception;

public class CompanyAlreadyExistsException extends DefaultException {

    public CompanyAlreadyExistsException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
