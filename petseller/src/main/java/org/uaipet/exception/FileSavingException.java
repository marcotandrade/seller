package org.uaipet.exception;

public class FileSavingException extends DefaultException {

    public FileSavingException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
