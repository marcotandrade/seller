package org.uaipet.exception;

public class CustomerAlreadyExistsException extends DefaultException {

    public CustomerAlreadyExistsException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
