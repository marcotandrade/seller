package org.uaipet.projection;

public interface OnGoingOrderProjection {

    String getStatus();
    String setStatus(String status);

    String getCreationDate();
    String setCreationDate(String creationDate);

    String getUserName();
    String setUserName(String userName);

    String getProductId();
    String setProductId(String productId);

    String getProductName();
    String setProductName(String productName);

    String getProductCount();
    String setProductCount(String productCount);

    String getKilogram();
    String setKilogram(String kilogram);

    String getTotalPrice();
    String setTotalPrice(String totalPrice);

    String getStreet();
    String setStreet(String street);

    String getNumber();
    String setNumber(String number);

    String getDistrict();
    String setDistrict(String district);

    String getComplement();
    String setComplement(String complement);

    String getCity();
    String setCity(String city);


    String getState();
    String setState(String state);


    String getPostalCode();
    String setPostalCode(String postalCode);

    String getOrderId();
    String setOrderId(String postalCode);

}
