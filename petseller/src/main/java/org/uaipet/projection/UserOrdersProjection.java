package org.uaipet.projection;

public interface UserOrdersProjection {

    Long getId();
    Long setId(Long id);

    String getDate();
    String setDate(String status);

    Long getOrderNumber();
    Long setOrderNumber(Long orderNumber);

    String getOrderStatus();
    String setOrderStatus(String orderStatus);

    String getCompanyName();
    String setCompanyName(String companyName);

    String getAmount();
    String setAmount(String amount);

    String getProductName();
    String setProductName(String productName);

    Integer getProductQtt();
    Integer setProductQtt(Integer productQtt);

    String getProductPrice();
    String setProductPrice(String productPrice);

    String getStreet();
    String setStreet(String street);

    Integer getNumber();
    Integer setNumber(Integer number);

    String getComplement();
    String setComplement(String complement);





}
