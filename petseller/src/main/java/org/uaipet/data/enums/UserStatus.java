package org.uaipet.data.enums;

public enum UserStatus {

    INACTIVE(0, "INACTIVE"),
    ACTIVE(1, "ACTIVE");


    private int cod;
    private String description;

    UserStatus(int cod, String description) {
        this.cod = cod;
        this.description = description;
    }

    public int getCod() {
        return cod;
    }

    public String getdescription () {
        return description;
    }

    public static UserStatus toEnum(Integer cod) {

        if (cod == null) {
            return null;
        }

        for (UserStatus x : UserStatus.values()) {
            if (cod.equals(x.getCod())) {
                return x;
            }
        }

        throw new IllegalArgumentException("Id inválido: " + cod);
    }

}