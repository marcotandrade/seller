package org.uaipet.data.repository;

import org.springframework.data.repository.CrudRepository;
import org.uaipet.data.model.AgeCategory;

public interface AgeCategoryRepository  extends CrudRepository<AgeCategory, Long> {

    AgeCategory findOneByName(String name);
}
