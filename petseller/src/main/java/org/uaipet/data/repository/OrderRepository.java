package org.uaipet.data.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.uaipet.data.model.OrderStatus;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.uaipet.projection.OnGoingOrderProjection;
import org.uaipet.projection.UserOrdersProjection;

import java.util.List;

public interface OrderRepository extends PagingAndSortingRepository<OrderStatus, Long> {


    @Query(value = "select po.id orderId, po.status status, po.created_date creationDate,\n" +
            "       us.full_name userName,p.id productId,\n" +
            "       pb.product_name productName, pb.product_quantity productCount,\n" +
            "       p.kilogram kilogram, pb.product_price totalPrice,\n" +
            "       ua.street, ua.number, ua.district, ua.complement,\n" +
            "       ua.city, ua.state, ua.postal_code postalCode\n" +
            "\n" +
            "from transaction tran\n" +
            "    inner join purchase_order po\n" +
            "        on tran.id = po.transaction_id\n" +
            "    inner join users us\n" +
            "        on tran.customer_id = us.id\n" +
            "    inner join product_basket pb on\n" +
            "        tran.id = pb.transaction_id\n" +
            "    inner join product p\n" +
            "        on pb.product_id = p.id\n" +
            "    inner join delivery_address da\n" +
            "        on po.id = da.order_id\n" +
            "    inner join user_address ua\n" +
            "        on da.user_address_id = ua.id\n" +
            "where tran.company_id = :merchantId \n" +
            "    and po.status <> 'FINISHED'",
            nativeQuery = true)
    List<OnGoingOrderProjection> listOnGoingOrders(@Param("merchantId") Long merchantId);


    @Query(value =
            "select tran.id,  " +
                    "       TO_CHAR(tran.created_date,'DD-MM-YYYY HH24:MI') as date,\n" +
                    "       os.id as orderNumber,\n" +
                    "       os.status as orderStatus,\n" +
                    "       c.nome_fantasia as companyName,\n" +
                    "       tran.amount as amount,\n" +
                    "       a.street as street,\n" +
                    "       a.number as number,\n" +
                    "       a.complement as complement\n" +
                    "\n" +
                    "from order_status os\n" +
                    "        inner join transaction tran on os.transaction_id = tran.id\n" +
                    "        inner join company c on tran.company_id = c.id\n" +
                    "        inner join delivery_address da on os.id = da.order_status_id\n" +
                    "        inner join address a on a.id = da.user_address_id\n" +
                    "where tran.customer_id = :userId", nativeQuery = true
    )
    Page<UserOrdersProjection> listUserOrders(@Param("userId") Long userId, Pageable pageable);


    @Query(value =
            "select\n" +
                    "       pb.product_name as productName,\n" +
                    "       pb.product_quantity as productQtt,\n" +
                    "       product_price as productPrice\n" +
                    "from order_status os\n" +
                    "        left join transaction tran on os.transaction_id = tran.id\n" +
                    "        left join company c on tran.company_id = c.id\n" +
                    "        left join transaction_product_basket tpb on tran.id = tpb.transaction_id\n" +
                    "        left join product_basket pb on tpb.product_basket_id = pb.id\n" +
                    "        left join delivery_address da on os.id = da.order_status_id\n" +
                    "        left join address a on a.id = da.user_address_id\n" +
                    "where os.id = :orderId", nativeQuery = true
    )
    List<UserOrdersProjection> listUserOrdersDetails(@Param("orderId") Long userId);

    @Query(value =
            "select tran.id,  " +
                    "       TO_CHAR(tran.created_date,'DD-MM-YYYY HH24:MI') as date,\n" +
                    "       os.id as orderNumber,\n" +
                    "       os.status as orderStatus,\n" +
                    "       c.nome_fantasia as companyName,\n" +
                    "       tran.amount as amount,\n" +
                    "       a.street as street,\n" +
                    "       a.number as number,\n" +
                    "       a.complement as complement\n" +
                    "\n" +
                    "from order_status os\n" +
                    "        inner join transaction tran on os.transaction_id = tran.id\n" +
                    "        inner join company c on tran.company_id = c.id\n" +
                    "        inner join delivery_address da on os.id = da.order_status_id\n" +
                    "        inner join address a on a.id = da.user_address_id\n" +
                    "where c.id = :companyId\n" +
                    "and os.status <> 'FINISHED'", nativeQuery = true
    )
    Page<UserOrdersProjection> listCompanyOrders(@Param("companyId") Long userId, Pageable pageable);

}
