package org.uaipet.data.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.uaipet.data.model.Address;


public interface AddressRepository extends CrudRepository<Address, Long> {

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Modifying
    @Query(value = "UPDATE ADDRESS SET DEFAULT_ADDRESS = false WHERE CUSTOMER_ID = ?1", nativeQuery = true)
    void updateAddressesNonDefault(Long userId);

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Modifying
    @Query(value = "UPDATE USER_ADDRESS SET DEFAULT_ADDRESS = true WHERE ID = ?1", nativeQuery = true)
    void setDefaultAddress(Long addressId);

}
