package org.uaipet.data.repository;

import org.springframework.data.repository.CrudRepository;
import org.uaipet.data.model.AnimalCategory;

public interface AnimalCategoryRepository extends CrudRepository<AnimalCategory, Long> {


}
