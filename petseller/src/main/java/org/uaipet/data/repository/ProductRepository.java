package org.uaipet.data.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.uaipet.data.model.AnimalCategory;
import org.uaipet.data.model.Company;
import org.uaipet.data.model.Product;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.uaipet.data.model.ProductCategory;

import java.util.List;
import java.util.Set;

public interface ProductRepository extends JpaRepository<Product, Long> {


    List<Product> findByCompanyIdAndStockCountGreaterThan(Long id, Long count);

    Product findByIdAndCompany(Long id, Company company);

    List<Product> findByNameContainingIgnoreCaseAndAnimalCategoryAndProductCategoryAndCompanyInAndStockCountGreaterThan(String name,
                                                                                                                        AnimalCategory animalCategory,
                                                                                                                        ProductCategory productCategory, Set<Company> companySet,
                                                                                                                        Long count, Pageable pageable);

    List<Product> findByNameContainingIgnoreCaseAndAnimalCategoryAndStockCountGreaterThan(String name,
                                                                                          AnimalCategory animalCategory,
                                                                                          Long count, Pageable pageable);

    List<Product> findByAnimalCategoryAndStockCountGreaterThan(AnimalCategory animalCategory,
                                                               Long count, Pageable pageable);

    List<Product> findByNameContainingIgnoreCaseAndCompanyIn(String name,Set<Company> companySet, Pageable pageable);

    Boolean existsByIdAndStockCountGreaterThanEqual(Long name, Long count);

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Modifying
    @Query(value = "UPDATE PRODUCT SET STOCK_COUNT = STOCK_COUNT -?1 WHERE ID = ?2", nativeQuery = true)
    void countDownProductStockQtt(Integer qtt, Long prodId);

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Modifying
    @Query(value = "UPDATE PRODUCT SET STOCK_COUNT = STOCK_COUNT +?1 WHERE ID = ?2", nativeQuery = true)
    void rollbackStockCountDown(Integer qtt, Long prodId);

    //pesquisa com search_index configurado

    List<Product> findByAnimalCategoryAndProductCategoryAndCompanyInAndStockCountGreaterThan(AnimalCategory animalCategory,
                                                                                          ProductCategory productCategory,
                                                                                          Set<Company> list, Long count, Pageable pageable);

    List<Product> findByNameContainingIgnoreCaseAndAnimalCategoryAndCompanyInAndStockCountGreaterThan(String name,
                                                                                                      AnimalCategory animalCategory,
                                                                                                      Set<Company> companySet,
                                                                                                      Long count, Pageable pageable);


}
