package org.uaipet.data.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.uaipet.data.model.Company;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.uaipet.data.model.User;

import java.util.List;
import java.util.Optional;

public interface CompanyRepository extends PagingAndSortingRepository<Company, Long> {

    List<Company> findByNomeFantasiaContainingIgnoreCase(String name);

    Optional<Company> findByIdAndCnpj(Long id, String cnpj);

    List<Company> findByAddressCity(String  cityName);

    @Modifying
    @Transactional(propagation = Propagation.NESTED)
    @Query(value = "UPDATE COMPANY SET FREIGHT_VALUE = :value WHERE ID = :merchant", nativeQuery = true)
    Integer updateFreight(Long merchant, Double value);


}
