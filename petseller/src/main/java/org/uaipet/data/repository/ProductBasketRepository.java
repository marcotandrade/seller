package org.uaipet.data.repository;

import org.uaipet.data.model.ProductBasket;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductBasketRepository extends PagingAndSortingRepository<ProductBasket, Long> {
}
