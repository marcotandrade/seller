package org.uaipet.data.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.uaipet.data.model.Customer;
import org.uaipet.dto.enums.StateEnum;

import java.util.Optional;
import java.util.Set;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    Customer findByExpoToken(String token);

    Optional<Customer> findById(Long id);

    Customer findCustomerByUser_Email(String email);

    Optional<Customer>  findCustomerByUserId(Long id);

    Set<Customer> findByAddressCityAndAddressState(String city, StateEnum state);

}
