package org.uaipet.data.repository;

import org.uaipet.data.model.DeliveryAddress;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface DeliveryAddressRepository  extends PagingAndSortingRepository<DeliveryAddress, Long> {
}
