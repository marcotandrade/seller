package org.uaipet.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import org.uaipet.data.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

    @Transactional(readOnly=true)
    User findByEmail(String email);
}
