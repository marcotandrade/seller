package org.uaipet.data.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;
import org.uaipet.data.model.DeliveryArea;
import org.uaipet.data.model.Company;

import java.util.List;

public interface DeliveryAreaRepository extends PagingAndSortingRepository<DeliveryArea, Long> {


    List<DeliveryArea> findByCompany(Company company);

    List<DeliveryArea> findByCompanyOrderByLongitudeAsc(Company company);

    @Transactional
    void deleteByCompany(Company company);

}
