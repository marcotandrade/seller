package org.uaipet.data.repository;

import org.springframework.data.repository.CrudRepository;
import org.uaipet.data.model.Customer;
import org.uaipet.data.model.ValidationToken;

public interface ValidationTokenRepository extends CrudRepository<ValidationToken, Long>  {

    ValidationToken findByCustomer(Customer customer);
}
