package org.uaipet.data.repository;

import org.springframework.data.repository.CrudRepository;
import org.uaipet.data.model.MeasurementCategory;

public interface MeasurementCategoryRepository extends CrudRepository<MeasurementCategory, Long> {

    MeasurementCategory findOneByName(String name);
}
