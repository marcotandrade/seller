package org.uaipet.data.repository;

import org.springframework.data.repository.CrudRepository;
import org.uaipet.data.model.ProductCategory;

public interface ProductCategoryRepository extends CrudRepository<ProductCategory, Long> {
}
