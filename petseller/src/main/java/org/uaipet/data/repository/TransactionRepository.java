package org.uaipet.data.repository;

import org.uaipet.data.model.Company;
import org.uaipet.data.model.Transaction;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;

public interface TransactionRepository extends PagingAndSortingRepository<Transaction, Long> {


    List<Transaction> findByCompanyAndInvoiceIsNullAndDateBetween(Company company, Date initialDate, Date finalDate);

}
