package org.uaipet.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.uaipet.data.model.Customer;
import org.uaipet.data.model.SearchIndex;
import org.uaipet.data.model.Address;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.uaipet.data.model.Company;

import java.util.List;

@Repository
public interface SearchIndexRepository extends JpaRepository<SearchIndex, Long> {


    List<SearchIndex> findByCustomerAndAddress(Customer customer, Address address);

    Boolean existsByCompany(Company company);

    @Transactional
    void deleteByCompany(Company company);


}
