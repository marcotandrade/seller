package org.uaipet.data.model;

import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Table(name = "SYSTEM_PARAMETERS")
@Data
public class SystemParameters extends DefaultEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", length = 20)
    private Long id;

    @Column(name = "KEY", length = 200, nullable = false)
    private String key;

    @Type(type = "jsonb")
    @Column(name = "VALUE", columnDefinition = "jsonb")
    private Object value;

    @Column(name = "DESCRIPTION", length = 255, nullable = false)
    private String description;

}
