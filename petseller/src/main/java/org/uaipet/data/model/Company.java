package org.uaipet.data.model;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@EqualsAndHashCode(callSuper = true)
@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
@Table(name = "COMPANY")
public class Company extends DefaultEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", length = 20)
    private Long id;

    @Column(nullable = false, name = "RAZAO_SOCIAL", length = 70)
    private String razaoSocial;

    @Column(nullable = false, name = "NOME_FANTASIA", length = 70)
    private String nomeFantasia;

    @Column(unique = true, nullable = false, name = "CNPJ", length = 17)
    private String cnpj;

    @Column(name = "FEE",  precision = 2, scale = 2, nullable = false)
    private Double fee;

    @Column(name = "MONTHLY_FEE",  precision = 2, scale = 2, nullable = false)
    private Double monthlyFee;

    @Column(name = "FREIGHT_VALUE", precision = 2, scale = 2)
    private Double freightValue;

    @Column(name = "PHONE_NUMBER", length = 15)
    private String phoneNumber;

    @Column(name = "IMAGE")
    private ArrayList<String> image;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ADDRESS_ID", referencedColumnName = "ID")
    private Address address;

    @JsonIgnore
    @OneToMany(mappedBy="company")
    private List<User> users;

    public Company(Long id) {
        this.id = id;
    }
}
