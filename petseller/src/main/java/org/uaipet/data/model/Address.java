package org.uaipet.data.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.uaipet.dto.enums.StateEnum;

import javax.persistence.*;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
@Table(name = "ADDRESS")
public class Address extends DefaultEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", length = 20)
    private Long id;

    @Column(name = "STATE", length = 2)
    @Enumerated(EnumType.STRING)
    private StateEnum state;

    @Column(name = "STREET", length = 100)
    private String street;

    @Column(name = "NUMBER", length = 6)
    private Long number;

    @Column(name = "DISTRICT", length = 60)
    private String district;

    @Column(name = "COMPLEMENT", length = 60)
    private String complement;

    @Column(name = "POSTAL_CODE", length = 8)
    private Long postalCode;

    @Column(name = "CITY", length = 60)
    private String city;

    @Column(name = "LATITUDE", precision = 6)
    private Double latitude;

    @Column(name = "LONGITUDE", precision = 6)
    private Double longitude;

    @Column(name = "DEFAULT_ADDRESS", length = 10)
    private Boolean defaultAddress;

    @OneToOne(mappedBy = "address")
    private Company company;

}
