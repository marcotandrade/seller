package org.uaipet.data.model;

import javax.persistence.*;

@Entity
@Table(name = "VALIDATION_TOKEN")
public class ValidationToken extends DefaultEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", length = 20)
    private Long id;

    @OneToOne
    @JoinColumn(name = "CUSTOMER_ID")
    private Customer customer;

    @Column(name = "TOKEN")
    private Integer token;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Customer getUser() {
        return customer;
    }

    public void setUser(Customer customer) {
        this.customer = customer;
    }

    public Integer  getToken() {
        return token;
    }

    public void setToken(Integer token) {
        this.token = token;
    }
}
