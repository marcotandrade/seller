package org.uaipet.data.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.uaipet.dto.enums.TransactionStatus;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@Entity
@Table(name = "TRANSACTION")
public class Transaction extends DefaultEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", length = 20)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE")
    private Date date;

    @Column(name = "AMOUNT", precision = 2, scale = 2)
    private Double amount;

    @ManyToOne
    @JoinColumn(name = "COMPANY_ID")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "INVOICE_ID")
    private Invoice invoice;

    @ManyToOne
    @JoinColumn(name = "CUSTOMER_ID")
    private Customer customer;

    @Enumerated(EnumType.STRING)
    @Column(name = "TRANSACTION_STATUS")
    private TransactionStatus transactionStatus;

    @OneToOne(mappedBy = "transaction" ,cascade = CascadeType.ALL)
    @JoinColumn(name="ORDER_STATUS_ID")
    private OrderStatus orderStatus;

    @OneToMany(cascade = CascadeType.ALL)
    private List<ProductBasket> productBasket;

}
