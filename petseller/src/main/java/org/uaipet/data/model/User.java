package org.uaipet.data.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.uaipet.data.enums.Profile;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
@Table( name = "UP_USER" )
public class User extends DefaultEntity implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "ID", length = 20)
    private Long id;

    @Column(name = "NAME", length = 60)
    private String name;

    @Column(name = "EMAIL", length = 60, unique = true)
    private String email;

    @Column(name = "TYPE", length = 2)
    private Integer type;

    @ToString.Exclude
    @Column(name = "PASSWORD", length = 300 )
    private String password;

    @Column(name = "STATUS", length = 10)
    private Integer status;

    @Column(name = "IMAGE")
    private ArrayList<String> image;

    @ElementCollection(fetch= FetchType.EAGER)
    @CollectionTable(name="PROFILES")
    private Set<Integer> profiles;

    @ManyToOne
    @JoinColumn(name="company_id", referencedColumnName = "ID")
    private Company company;

    @OneToOne(mappedBy = "user")
    private Customer customer;

    public Set<Profile> getProfiles() {
        return this.profiles.stream().map(Profile::toEnum).collect(Collectors.toSet());
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return (Collection<GrantedAuthority>)(Collection<?>) this.profiles;
    }
}