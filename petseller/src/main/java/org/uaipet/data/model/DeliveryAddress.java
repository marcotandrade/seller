package org.uaipet.data.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Builder
@Table(name = "DELIVERY_ADDRESS")
public class DeliveryAddress extends DefaultEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", length = 20)
    private Long id;

    @OneToOne
    @JoinColumn(name = "USER_ADDRESS_ID")
    private Address address;

    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    private OrderStatus orderStatus;


}