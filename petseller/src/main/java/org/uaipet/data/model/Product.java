package org.uaipet.data.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "PRODUCT")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product extends DefaultEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", length = 20)
    private Long id;

    @Column(nullable = false, name = "NAME", length = 70)
    private String name;

    @ManyToOne
    @JoinColumn(name = "PRODUCT_CATEGORY_ID")
    private ProductCategory productCategory;

    @ManyToOne
    @JoinColumn(name = "ANIMAL_CATEGORY_ID")
    private AnimalCategory animalCategory;

    @ManyToOne
    @JoinColumn(name = "MEASUREMENT_CATEGORY_ID")
    private MeasurementCategory measurementUnit;

    @ManyToOne
    @JoinColumn(name = "AGE_CATEGORY_ID")
    private AgeCategory ageCategory;


    @Column(name = "PRICE", precision = 2)
    private Double price;

    @ManyToOne
    @JoinColumn(name = "COMPANY_ID")
    private Company company;

    @Column(name = "MANUFACTURER", length = 70)
    private String manufacturer;

    @Column(name = "STOCK_COUNT", length = 5)
    private Long stockCount;

    @Column(name = "DESCRIPTION")
    private byte[] description;

    @Column(name = "IMAGE")
    private ArrayList<String> image;

    @Column(name = "MEASUREMENT_UNIT_VALUE")
    private String measurementUnitValue;

}
