package org.uaipet.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "INVOICE")
public class Invoice extends DefaultEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", length = 20)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "COMPANY_ID")
    private Company company;

    @Column(name = "AMOUNT", precision = 10, scale = 2, nullable = false)
    private Double amount;

    @Column(name = "LIQUID_AMOUNT", precision = 10, scale = 2, nullable = false)
    private Double liquidAmount;

    @Column(name = "INITIAL_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date initialDate;

    @Column(name = "END_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date finalDate;

    @Column(name = "PAYED")
    private boolean payed;

    @Column(name = "COD_PAYMENT_TRANSACTION", length = 300)
    private String codPaymentTransaction;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Company getMerchant() {
        return company;
    }

    public void setMerchant(Company company) {
        this.company = company;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getLiquidAmount() {
        return liquidAmount;
    }

    public void setLiquidAmount(Double liquidAmount) {
        this.liquidAmount = liquidAmount;
    }

    public Date getInitialDate() {
        return initialDate;
    }

    public void setInitialDate(Date initialDate) {
        this.initialDate = initialDate;
    }

    public Date getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(Date finalDate) {
        this.finalDate = finalDate;
    }

    public boolean isPayed() {
        return payed;
    }

    public void setPayed(boolean payed) {
        this.payed = payed;
    }


    public String getCodPaymentTransaction() {
        return codPaymentTransaction;
    }

    public void setCodPaymentTransaction(String codPaymentTransaction) {
        this.codPaymentTransaction = codPaymentTransaction;
    }
}
