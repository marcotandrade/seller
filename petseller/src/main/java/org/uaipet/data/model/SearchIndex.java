package org.uaipet.data.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
@Table(name = "SEARCH_INDEX")
public class SearchIndex extends DefaultEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", length = 20)
    private Long id;

    @ManyToOne
    @JoinColumn( name = "CUSTOMER_ID")
    private Customer customer;

    @ManyToOne
    @JoinColumn( name = "COMPANY_ID")
    private Company company;

    @ManyToOne
    @JoinColumn( name = "ADDRESS_ID")
        private Address address;

}
