package org.uaipet.data.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.uaipet.dto.enums.OrderStatusEnum;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Getter
@Setter
@Table(name = "ORDER_STATUS")
public class OrderStatus extends DefaultEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", length = 20)
    private Long id;

    @OneToOne
    @JoinColumn(name="TRANSACTION_ID", nullable=false, updatable = false)
    private Transaction transaction;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private OrderStatusEnum orderStatusEnum;


}
