package org.uaipet.crypto;


import com.amazonaws.encryptionsdk.AwsCrypto;
import com.amazonaws.encryptionsdk.CryptoResult;
import com.amazonaws.encryptionsdk.kms.KmsMasterKey;
import com.amazonaws.encryptionsdk.kms.KmsMasterKeyProvider;

import java.nio.charset.StandardCharsets;

public class KmsEncryptTest {

    private static final byte[] EXAMPLE_DATA = "Hello World".getBytes(StandardCharsets.UTF_8);

    public static void main(String[] args)  {

        final AwsCrypto crypto = new AwsCrypto();

        String keyArn = "arn:aws:kms:sa-east-1:570022617540:key/4ace5233-8cfd-4b87-8e8b-a9065a1d3e9b";

        // 2. Instantiate a KMS master key provider
        final KmsMasterKeyProvider masterKeyProvider = KmsMasterKeyProvider.builder().withKeysForEncryption(keyArn)
                .build();

        // 4. Encrypt the data
        final CryptoResult<byte[], KmsMasterKey> encryptResult = crypto.encryptData(masterKeyProvider, EXAMPLE_DATA);
        final byte[] ciphertext = encryptResult.getResult();

        // 5. Decrypt the data
        final CryptoResult<byte[], KmsMasterKey> decryptResult = crypto.decryptData(masterKeyProvider, ciphertext);

        // 6. Before verifying the plaintext, verify that the customer master key that
        // was used in the encryption operation was the one supplied to the master key provider.
        if (!decryptResult.getMasterKeyIds().get(0).equals(keyArn)) {
            throw new IllegalStateException("Wrong key ID!");
        }

        // 8. Verify that the decrypted plaintext matches the original plaintext
        String s = new String(decryptResult.getResult(), StandardCharsets.UTF_8);
        assert  s.equalsIgnoreCase("numerodocarta+cvv+validade+cpf+nome");



    }



}
