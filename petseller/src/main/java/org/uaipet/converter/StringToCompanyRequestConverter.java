package org.uaipet.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.uaipet.dto.request.CompanyRequest;
import org.uaipet.dto.request.ProductRequest;

import java.io.IOException;

@Component
public class StringToCompanyRequestConverter implements Converter<String, CompanyRequest> {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public CompanyRequest convert(String source)  {
        try {
            return objectMapper.readValue(source, CompanyRequest.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
