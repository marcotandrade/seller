package org.uaipet.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.uaipet.dto.request.CustomerRequest;

import java.io.IOException;

@Component
public class StringToCustomerRequestConverter implements Converter<String, CustomerRequest> {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public CustomerRequest convert(String source)  {
        try {
            return objectMapper.readValue(source, CustomerRequest.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
