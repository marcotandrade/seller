package org.uaipet.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.uaipet.dto.request.ProductRequest;

import java.io.IOException;

@Component
public class StringToProductRequestConverter implements Converter<String, ProductRequest> {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public ProductRequest convert(String source)  {
        try {
            return objectMapper.readValue(source, ProductRequest.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
