package org.uaipet.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Service
public class S3StorageService {

    private static final Logger log = LoggerFactory.getLogger(S3StorageService.class);

    @Value("${cloud.aws.s3.bucket.name}")
    String s3bucket;

    @Autowired
    private AmazonS3 amazonS3;


    public ArrayList<String> uploadImageFile(List<MultipartFile> images, String imageName,String folderName) throws IOException {
        log.info("M: uploadImageFile");
        ArrayList<String> urlList = new ArrayList<>();
        Integer count = 1;
            for (MultipartFile document : images) {
                ObjectMetadata metadata = new ObjectMetadata();
                metadata.setContentType(document.getContentType());
                metadata.setContentLength(document.getSize());

                InputStream inputStream = document.getInputStream();

                log.info(String.format("s3bucket %s - fileName %s ", s3bucket, imageName));
                String key = formatS3Key(folderName,imageName + count.toString());
                PutObjectRequest request = new PutObjectRequest(s3bucket, key, inputStream, metadata)
                        .withCannedAcl(CannedAccessControlList.PublicRead);

                amazonS3.putObject(request);
                URL url = amazonS3.getUrl(s3bucket, key);
                log.info(String.format("URL returned: %s", url.toExternalForm()));
                urlList.add(url.toExternalForm());
                count++;
            }
        return urlList;
    }

    private String formatS3Key(String folderName,String name) {
        return  folderName + name.replaceAll("\\s+", "");
    }


}
