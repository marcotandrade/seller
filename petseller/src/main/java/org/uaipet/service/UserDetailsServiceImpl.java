package org.uaipet.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.uaipet.data.model.Company;
import org.uaipet.data.model.User;
import org.uaipet.data.repository.UserRepository;
import org.uaipet.security.UserSS;

import java.util.Objects;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {


    @Autowired
    private UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = repository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException(email);
        }

        Company company = user.getCompany();
        return
                new UserSS(
                        user.getId(),
                        user.getName(),
                        user.getEmail(),
                        user.getPassword(),
                        user.getImage(),
                        user.getProfiles(),
                        user.getId(), Objects.nonNull(company) ? company.getId() : null);
    }
}
