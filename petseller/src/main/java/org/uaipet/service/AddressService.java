package org.uaipet.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.uaipet.dto.UserAddressDTO;
import org.uaipet.data.model.Address;
import org.uaipet.data.repository.AddressRepository;

@Service
public class AddressService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyService.class);

    @Autowired
    private AddressRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Address save(Address address){
        LOGGER.info("createAddress initialized" + address.toString());

       return this.repository.save(address);
    }

    @Transactional(rollbackFor = Exception.class)
    public Boolean setDefaultAddress(UserAddressDTO userAddress){
        LOGGER.info("setDefaultAddress initialized" + userAddress.toString());
        this.repository.updateAddressesNonDefault(userAddress.getCustomerId());
        this.repository.setDefaultAddress(userAddress.getAddressId());
        return true;
    }

}
