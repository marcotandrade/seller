package org.uaipet.service.exceptions;

public class AddressException extends RuntimeException {

    public AddressException(String msg) {
        super(msg);
    }

    public AddressException(String msg, Throwable cause) {
        super(msg, cause);
    }

}