package org.uaipet.service.exceptions;

public class ProductFromDifferentStoreException extends RuntimeException {

    public ProductFromDifferentStoreException() {
        super();
    }

    public ProductFromDifferentStoreException(String s) {
        super(s);
    }
}
