package org.uaipet.service.exceptions;

public class OutOfDeliveryRangeException extends RuntimeException{

    public OutOfDeliveryRangeException() {
        super();
    }

    public OutOfDeliveryRangeException(String s) {
        super(s);
    }
}
