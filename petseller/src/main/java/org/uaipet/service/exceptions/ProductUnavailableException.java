package org.uaipet.service.exceptions;

public class ProductUnavailableException extends RuntimeException {

    public ProductUnavailableException(String s) {
        super(s);
    }

    public ProductUnavailableException() {
        super();
    }
}
