package org.uaipet.service;

import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.uaipet.data.model.User;
import org.uaipet.dto.AddressDTO;
import org.uaipet.dto.CustomerDTO;
import org.uaipet.dto.enums.RegistrationEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;
import org.uaipet.client.GeoCodingClient;
import org.uaipet.dto.request.CustomerRequest;
import org.uaipet.dto.request.ValidateCustomerRequest;
import org.uaipet.exception.CustomerAlreadyExistsException;
import org.uaipet.exception.CustomerException;
import org.uaipet.data.model.Customer;
import org.uaipet.data.model.ValidationToken;
import org.uaipet.data.repository.CustomerRepository;
import org.uaipet.exception.CustomerNotFoundException;
import org.uaipet.exception.FileSavingException;
import org.uaipet.exception.InvalidAddressException;
import org.uaipet.generated.GeoCodingResponse;
import org.uaipet.data.model.Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.uaipet.data.repository.ValidationTokenRepository;
import org.uaipet.security.JWTUtil;
import org.uaipet.service.exceptions.AddressException;
import org.uaipet.utils.Validation;

import java.io.IOException;
import java.util.*;

import static org.uaipet.dto.utils.Builder.*;


@Service
public class CustomerService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerService.class);

    @Autowired
    private CustomerRepository repository;

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private ValidationTokenRepository validationTokenRepository;

    @Autowired
    private S3StorageService s3StorageService;

    @Autowired
    private GeoCodingClient geoCodingClient;

    @Value("${cloud.google.geo-coding-api.key}")
    private String googleApiKey;

    @Autowired
    private AddressService addressService;

    @Autowired
    private SearchIndexService searchIndexService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private Validation validation;

    @Autowired
    private JWTUtil jwtUtil;

    @Transactional(rollbackFor = Exception.class)
    public ResponseEntity save(CustomerRequest customerRequest,List<MultipartFile> images) throws CustomerException {

        Customer customer = builderCustomer(customerRequest);

        if(validation.custumerObjectInfo(customerRequest) == RegistrationEnum.INCOMPLETE){
            return  ResponseEntity.ok(this.repository.save(customer));
        }

        User alreadyInUse = this.userService.findByEmail(customerRequest.getEmail());

        if (Objects.nonNull(alreadyInUse)) {
            throw new CustomerAlreadyExistsException("E-mail já utilizado em outro cadastro!");
        }

        validateAddress(customerRequest);

        Address address = buildAddress(customerRequest);
        GeoCodingResponse latlon = geoCodingClient.fetchGeoLocationInfo(getAddressOnApiFormat(address), googleApiKey);
        address.setLatitude(latlon.getResults().get(0).getGeometry().getLocation().getLat());
        address.setLongitude(latlon.getResults().get(0).getGeometry().getLocation().getLng());
        address = this.addressService.save(address);
        List<AddressDTO> addressDTOList = new ArrayList<>();
        addressDTOList.add(new AddressDTO(address));

        customer.getUser().setPassword(encoder.encode(customer.getUser().getPassword()));
        customer.setUser(this.userService.save(customer.getUser()));
        customer.setAddress(Collections.singletonList(address));

        saveCustomerPicture(images, customer);
        Customer addedCustomer = this.repository.save(customer);

        createValidationToken(customerRequest);
        searchIndexService.saveSearchIndex(address, null, addedCustomer.getId());

        Collection<? extends GrantedAuthority> authorities = (Collection<GrantedAuthority>)(Collection<?>) addedCustomer.getUser().getProfiles();
        String token = jwtUtil.generateToken(addedCustomer.getUser().getEmail(), authorities,
                addedCustomer.getUser().getId(), customer.getUser().getName(), customer.getUser().getImage(), null);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization","Bearer " + token);
        headers.add("access-control-expose-headers", "Authorization");

       return ResponseEntity.ok().headers(headers).build();
    }

    private void validateAddress(CustomerRequest customerRequest) {
        if(Objects.isNull(customerRequest.getAddressNumber()) ||
                Objects.isNull(customerRequest.getDistrict()) ||
                Objects.isNull(customerRequest.getCity()) ||
                Objects.isNull(customerRequest.getStreet()) ||
                Objects.isNull(customerRequest.getState()) ||
                Objects.isNull(customerRequest.getPostalCode()) ){
            throw new InvalidAddressException("Dados incompletos informados no endereço");
        }
    }


    @Transactional(rollbackFor = Exception.class)
    public Customer update(Long id, CustomerDTO dto) throws CustomerNotFoundException {

        Optional<Customer> customer = this.repository.findById(id);
        Customer customerSave = customer.orElse(null);

        if(Objects.isNull(customerSave)){
            LOGGER.error("Erro ao alterar Custumer!");
            throw new CustomerNotFoundException("Usuário não encontrado!");
        }

        userService.requestUserIdEqualsTokenUserId(id);

        //copia as propriedades do objeto 1 para o 2 ignorando o ID
        BeanUtils.copyProperties( builderCustomer(dto) , customerSave, "id", "user", "expoToken");
        this.updateAddress(id, customerSave.getAddress());
        return this.repository.save( customerSave );
    }


    public boolean userValidation(ValidateCustomerRequest validateCustomerRequest){
        userService.requestUserIdEqualsTokenUserId(validateCustomerRequest.getUserId());
        try {
            Optional<Customer> customer = repository.findCustomerByUserId(validateCustomerRequest.getUserId());
            ValidationToken validationToken = validationTokenRepository.findByCustomer(customer.get());
            if(validationToken.getToken().equals(Integer.valueOf(validateCustomerRequest.getToken()))) {
                customer.get().setValid(true);
                repository.save(customer.get());
                validationTokenRepository.delete(validationToken);
                return true;
            }
            return false;
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(),ex);
            return false;
        }
    }

    public void saveCustomerPicture(List<MultipartFile> images, Customer customer) {
        LOGGER.info(String.format("saveCustomerPicture email %s; customerId %s",customer.getUser().getEmail(), customer.getId()));
        if(!images.isEmpty()){
            try {
                ArrayList<String> imagesUrl = s3StorageService.uploadImageFile(images, customer.getUser().getEmail() + "-picture", "customer-images/");
                customer.getUser().setImage(imagesUrl);
            }catch (IOException ex){
                throw new FileSavingException("Error saving image");
            }
        }
    }


    public List<Address> updateAddress(Long id, List<Address> list) {

        Optional<Customer> customer = this.repository.findById(id);
        Customer customerSave = customer.orElse(null);

        if(Objects.isNull(customerSave)){
            LOGGER.error("Erro ao atualizar endereço, usuário não encontrado com o id: " + id);
            throw new CustomerNotFoundException("Usuário não encontrado!");
        }

        List<Address> addressList = new ArrayList<>();
        list.forEach(address -> {
            if (Objects.isNull(address) || validAddressRequest(address)) {
                LOGGER.error("Erro ao salvar o endereço: " + address.toString());
                throw new AddressException("As informações do endereço não podem ser nulas");
            }
            GeoCodingResponse latlon = geoCodingClient.fetchGeoLocationInfo(getAddressOnApiFormat(address), googleApiKey);
            address.setLatitude(latlon.getResults().get(0).getGeometry().getLocation().getLat());
            address.setLongitude(latlon.getResults().get(0).getGeometry().getLocation().getLng());
            Address addressSave = this.addressService.save(address);
            searchIndexService.saveSearchIndex(addressSave, null, id);
            addressList.add(addressSave);
        });

        return addressList;

    }

    private boolean validAddressRequest(Address address) {
       return StringUtils.isBlank(address.getStreet()) &&
              StringUtils.isBlank(address.getNumber().toString()) &&
              StringUtils.isBlank(address.getCity());
    }

    private String getAddressOnApiFormat(Address address) {
        return address.getStreet().concat("+").concat(address.getNumber().toString())
                .concat("+").concat(address.getCity());
    }


    @Async
    public void createValidationToken(CustomerRequest customerRequest ) {
        ResponseEntity response;
        long startTime = System.currentTimeMillis();
        LOGGER.info("M createValidationToken initialized!");
        try {
            Customer us = this.repository.findCustomerByUser_Email(customerRequest.getEmail());
            if (us != null) {
                ValidationToken pass = tokenService.generateToken(us);
                //send token to e-mail
                emailService.sendSimpleMessage(us.getUser().getEmail(),"UaiPetOrg - Confirmação de cadastro",
                        "Email teste com token criado: " + pass.getToken());
            } else {
                LOGGER.warn("Usuario não encontrado!");
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
        }
        LOGGER.info(String.format("createValidationToken Finalizado em %s ms!", System.currentTimeMillis() - startTime));
    }

    public Customer findById(Long id) {
        userService.requestUserIdEqualsTokenUserId(id);

        Optional<User> user = userService.findById(id);
        Optional<Customer> customer = Optional.empty();
        if (user.isPresent()) {
            customer = this.repository.findCustomerByUserId(user.get().getId());
            if (!customer.isPresent()) {
                throw new CustomerNotFoundException("Usuario não encontrado com o id: " + id);
            }
        }
        customer.get().getUser().setPassword(null);
        return customer.get();
    }
}
