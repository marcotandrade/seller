package org.uaipet.service;

import org.hibernate.jdbc.Expectation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.uaipet.data.enums.Profile;
import org.uaipet.data.model.User;
import org.uaipet.data.repository.UserRepository;
import org.springframework.web.multipart.MultipartFile;
import org.uaipet.dto.request.CompanyRequest;
import org.uaipet.dto.request.FreightValueRequest;
import org.uaipet.dto.request.UserRequest;
import org.uaipet.exception.*;
import org.uaipet.data.model.Company;
import org.uaipet.data.repository.AddressRepository;
import org.uaipet.data.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.uaipet.utils.Validation;

import java.io.IOException;
import java.util.*;

import static org.uaipet.dto.utils.Builder.builderCompany;
import static org.uaipet.dto.utils.Builder.builderUser;


@Service
public class CompanyService {

    private static final Logger log = LoggerFactory.getLogger(CompanyService.class);

    @Autowired
    private CompanyRepository repository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private S3StorageService s3StorageService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private Validation validate;

    @Value("${company.default.fee}")
    private String fee;

    @Value("${company.default.monthlyFee}")
    private String monthlyFee;

    @Transactional(rollbackFor = Exception.class)
    public Company createCompany(CompanyRequest companyRequest, List<MultipartFile> images) throws CompanyException {

        Company company = builderCompany(companyRequest);

        if(!validate.companyObjectInfo(company)) {
            throw new CompanyException("Dados incorretos");
        }

        User user = company.getUsers().get(0);
        user.setPassword(encoder.encode(user.getPassword()));

        company.setAddress(this.addressRepository.save(company.getAddress()));
        company.setFee(Double.valueOf(fee));
        company.setMonthlyFee(Double.valueOf(monthlyFee));

        Company persistedCompany = this.repository.save(saveCompanyPicture(images, company));
        user.setCompany(new Company(persistedCompany.getId()));

        try {
            this.userRepository.save(user);
        } catch (Exception e){

        }

//        persistedCompany.setUsers();
        return persistedCompany;
    }

    public ResponseEntity<String> saveFreightValue(FreightValueRequest freightValueRequest) throws CompanyNotFoundException {
        Boolean existsCompany = repository.existsById(freightValueRequest.getCompanyId());
        if(existsCompany) {
            repository.updateFreight(freightValueRequest.getCompanyId(),
                    freightValueRequest.getFreightValue());
            return ResponseEntity.ok().body("Freight value updated");
        }
        throw new CompanyNotFoundException("Company not found by ID and Email");
    }

    public List<Company> findCompanyByName(String name) throws CompanyException {
        try {
            return repository.findByNomeFantasiaContainingIgnoreCase(name);
        } catch (Exception ex) {
            throw new CompanyException(ex.getMessage());
        }

    }

    public Company saveCompanyPicture(List<MultipartFile> images,
                                      Company company) {
        ArrayList<String> imagesUrl = new ArrayList<>();
        if(!images.isEmpty()){
            try {
                imagesUrl = s3StorageService.uploadImageFile(images, company.getCnpj() + "-picture", "company-images/");

            }catch (IOException ex){
                throw new FileSavingException("Error saving image");
            }
        }
        company.getUsers().get(0).setImage(imagesUrl);
        company.setImage(imagesUrl);
        return company;
    }

    public void saveNewUser(Long id,List<UserRequest> userRequests) {

        if(Objects.isNull(id)){
            throw new CompanyException("Company ID cannot be null");
        }

        Optional<Company> company = this.repository.findById(id);

        if(!company.isPresent()){
            throw new CompanyNotFoundException(String.format("Company with id: %s cannot be null", id));
        }

        if(Objects.isNull(userRequests) || userRequests.isEmpty()) {
            throw new UserNotFoundException("Users cannot be null");
        }

        List<User> users = new ArrayList<>();

        userRequests.forEach(user -> users.add(builderUser(user, company.get(), encoder)));

        this.userRepository.saveAll(users);

    }

}
