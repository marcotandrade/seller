package org.uaipet.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.uaipet.dto.request.DeliveryAreaRequest;
import org.uaipet.data.model.Company;
import org.uaipet.data.model.DeliveryArea;
import org.uaipet.data.repository.CompanyRepository;
import org.uaipet.data.repository.DeliveryAreaRepository;

import java.util.Optional;


@Service
public class DeliveryAreaService {
    private static final Logger log = LoggerFactory.getLogger(DeliveryAreaService.class);

    @Autowired
    private DeliveryAreaRepository deliveryAreaRepository;

    @Autowired
    private SearchIndexService searchIndexService;

    @Autowired
    private CompanyRepository companyRepository;


    public Boolean registerDeliveryArea(DeliveryAreaRequest deliveryArea) {
        log.info(String.format("Method: registerDeliveryArea - %s", deliveryArea.toString()));
        deleteDeliveryArea(deliveryArea.getCompanyId());
        Optional<Company> company = companyRepository.findById(deliveryArea.getCompanyId());

        if(company.isPresent()) {
            deliveryArea.getLocations().forEach(local -> {
                DeliveryArea area = DeliveryArea.builder()
                        .company(company.get())
                        .latitude(local.getLat())
                        .longitude(local.getLng())
                        .build();

                deliveryAreaRepository.save(area);
            });

            searchIndexService.saveSearchIndex(null, deliveryArea, deliveryArea.getCompanyId());
            return true;
        }
        return false;
    }


    public void deleteDeliveryArea(Long companyId) {
        log.info(String.format("Method: deleteDeliveryArea - %s", companyId));
        Optional<Company> company = companyRepository.findById(companyId);
        company.ifPresent(comp -> {
            deliveryAreaRepository.deleteByCompany(comp);
        });
    }


}
