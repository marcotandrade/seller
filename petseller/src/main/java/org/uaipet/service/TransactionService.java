package org.uaipet.service;

import org.uaipet.exception.TransactionException;
import org.uaipet.data.model.Company;
import org.uaipet.data.model.Transaction;
import org.uaipet.data.repository.CompanyRepository;
import org.uaipet.data.repository.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TransactionService {

    private static final Logger log = LoggerFactory.getLogger(TransactionService.class);

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private CompanyRepository companyRepository;

    public List<Transaction> listUnPayedTransactions(Long merchant, Date initialDate, Date finalDate) throws TransactionException {
        List<Transaction> tran = new ArrayList<>();
        try {
            Optional<Company> mer = companyRepository.findById(merchant);
            if (mer.isPresent()) {
                tran = transactionRepository.findByCompanyAndInvoiceIsNullAndDateBetween(mer.get(), initialDate, finalDate);
            }
        } catch (Exception ex) {
            log.error("Erro ao buscar por transações não pagas!");
            throw new TransactionException(ex);
        }
        return tran;
    }


}
