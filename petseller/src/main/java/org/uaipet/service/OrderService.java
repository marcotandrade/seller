package org.uaipet.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.uaipet.data.model.Customer;
import org.uaipet.data.repository.CustomerRepository;
import org.uaipet.dto.AddressDTO;
import org.uaipet.dto.CompanyDTO;
import org.uaipet.dto.UserOrderProductsDTO;
import org.uaipet.dto.UserOrdersDTO;
import org.uaipet.dto.enums.OrderStatusEnum;
import org.uaipet.data.model.Company;
import org.uaipet.data.model.OrderStatus;
import org.uaipet.dto.request.UserOrdersRequest;
import org.uaipet.projection.OnGoingOrderProjection;
import org.uaipet.data.repository.CompanyRepository;
import org.uaipet.data.repository.OrderRepository;
import org.uaipet.data.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.uaipet.data.repository.TransactionRepository;
import org.uaipet.projection.UserOrdersProjection;
import org.uaipet.security.UserSS;
import org.uaipet.service.exceptions.AuthorizationException;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
public class OrderService {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderService.class);
    public static final int PAGE_SIZE = 5;
    public static final int COMPANY_PAGE_SIZE = 20;


    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private CustomerService customerService;

    public void updateToNextStatus(OrderStatus orderStatus) {
        switch (orderStatus.getOrderStatusEnum()) {
            case WAITING_CONFIRMATION:
                orderStatus.setOrderStatusEnum(OrderStatusEnum.PREPARING_PRODUCT);
                break;
            case PREPARING_PRODUCT:
                orderStatus.setOrderStatusEnum(OrderStatusEnum.SENT_TO_DELIVERY);
                break;
            case SENT_TO_DELIVERY:
                orderStatus.setOrderStatusEnum(OrderStatusEnum.FINISHED);
                break;
            case FINISHED:
                break;
            default:
                orderStatus.setOrderStatusEnum(OrderStatusEnum.WAITING_CONFIRMATION);
        }
        orderRepository.save(orderStatus);
    }

    public List<OnGoingOrderProjection> listOnGoingOrders(Long merchant) {
        return orderRepository.listOnGoingOrders(merchant);
    }

    public CompanyDTO getMerchantIfoToReportProblem(Long orderId) {
        Optional<OrderStatus> order = orderRepository.findById(orderId);
        CompanyDTO companyDTO = CompanyDTO.builder().build();
        if (order.isPresent()) {
            Company company = order.get().getTransaction().getCompany();
            companyDTO.setNomeFantasia(company.getNomeFantasia());
            companyDTO.setAddress(new AddressDTO(company.getAddress()));
            companyDTO.setPhoneNumber(company.getPhoneNumber());
        }
        return companyDTO;
    }

    public List<UserOrdersDTO> getCompanyOrders(UserOrdersRequest ordersRequest){
        long startTime = System.currentTimeMillis();
        LOGGER.info("M getCompanyOrders");
        Pageable page = PageRequest.of(ordersRequest.getPage(), COMPANY_PAGE_SIZE);
        List<UserOrdersDTO> response =  convertToOrderDTO(orderRepository.listCompanyOrders(ordersRequest.getCompanyId(), page).stream().collect(Collectors.toList()));
        LOGGER.info(String.format("M getCompanyOrders Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return response;
    }

    public List<UserOrdersDTO> getUserOrders(UserOrdersRequest ordersRequest) {
        long startTime = System.currentTimeMillis();
        LOGGER.info("M getUserOrders");
        Customer customer = customerService.findById(ordersRequest.getUserId());
        Pageable page = PageRequest.of(ordersRequest.getPage(), PAGE_SIZE);
        List<UserOrdersDTO> response = convertToOrderDTO(orderRepository.listUserOrders(customer.getId(), page).stream().collect(Collectors.toList()));
        LOGGER.info(String.format("M getUserOrders Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return response;
    }

    private List<UserOrdersDTO> convertToOrderDTO(List<UserOrdersProjection> listUserOrders) {
        List<UserOrdersDTO> userOrdersDTOS = new ArrayList<>();
        for (UserOrdersProjection order : listUserOrders) {
            UserOrdersDTO ordersDTO = UserOrdersDTO.builder()
                    .addressNumber(order.getNumber())
                    .street(order.getStreet())
                    .complement(order.getComplement())
                    .companyName(order.getCompanyName())
                    .amount(order.getAmount())
                    .orderNumber(order.getOrderNumber())
                    .orderStatus(order.getOrderStatus())
                    .date(order.getDate())
                    .build();

            List<UserOrderProductsDTO> userOrderProductsDTOS = new ArrayList<>();
            List<UserOrdersProjection> details = orderRepository.listUserOrdersDetails(ordersDTO.getOrderNumber());
            details.forEach(detail -> {
                userOrderProductsDTOS.add(UserOrderProductsDTO.builder()
                        .productName(detail.getProductName())
                        .productPrice(detail.getProductPrice())
                        .productQtt(detail.getProductQtt())
                        .build());
            });
            ordersDTO.setUserOrderProducts(userOrderProductsDTOS);
            userOrdersDTOS.add(ordersDTO);
        }
        LOGGER.info(" M getUserOrders finalizado");
        return userOrdersDTOS;
    }

}
