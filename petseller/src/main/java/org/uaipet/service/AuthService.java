package org.uaipet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.uaipet.data.model.User;
import org.uaipet.security.UserSS;
import org.uaipet.service.exceptions.AuthorizationException;
import org.uaipet.service.exceptions.ObjectNotFoundException;

import java.util.Objects;
import java.util.Random;

@Service
public class AuthService {

    private final UserService service;

    private final BCryptPasswordEncoder encoder;

    private final EmailService emailService;

    @Autowired
    public AuthService( UserService service,
                        BCryptPasswordEncoder encoder,
                        EmailService emailService) {

        this.service = service;
        this.encoder = encoder;
        this.emailService = emailService;

    }

    private Random rand = new Random();

    public void sendNewPassword(String email) {

        User user  = this.service.findByEmail(email);

        if (Objects.isNull(user)) {
            throw new ObjectNotFoundException("Email não encontrado");
        }

        String newPass = newPassword();
        user.setPassword(encoder.encode(newPass));

        this.service.save(user);
        emailService.sendNewPasswordEmail(user, newPass);
    }

    private String newPassword() {
        char[] vet = new char[10];
        for (int i=0; i<10; i++) {
            vet[i] = randomChar();
        }
        return new String(vet);
    }

    private char randomChar() {
        int opt = rand.nextInt(3);
        if (opt == 0) {
            return (char) (rand.nextInt(10) + 48);
        }
        else if (opt == 1) {
            return (char) (rand.nextInt(26) + 65);
        }
        else {
            return (char) (rand.nextInt(26) + 97);
        }
    }
}
