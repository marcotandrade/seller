package org.uaipet.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.uaipet.dto.CoordinateDTO;
import org.uaipet.dto.request.DeliveryAreaRequest;
import org.uaipet.data.model.*;
import org.uaipet.data.repository.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class SearchIndexService {

    private static final Logger log = LoggerFactory.getLogger(SearchIndexService.class);

    @Autowired
    private SearchIndexRepository searchIndexRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private DeliveryAreaRepository deliveryAreaRepository;

    @Async
    public void saveSearchIndex(Address address, DeliveryAreaRequest deliveryArea, Long entityId) {
        try {
            log.info("Method: saveSearchIndex");
            if (address != null) {
                createIndexFromUserAddress(address, entityId);
            } else if (deliveryArea != null) {
                deletePreviousArea(deliveryArea);
                createIndexFromMerchantAddress(deliveryArea);
            }
        } catch (Exception ex) {
            log.error("Erro ao cadastrar search index - " + address != null ? address.toString() : deliveryArea.getCompanyId().toString());
        }
    }


    private void createIndexFromMerchantAddress(DeliveryAreaRequest deliveryArea) {
        log.info("Method: createIndexFromMerchantAddress");

        Optional<Company> company = companyRepository.findById(deliveryArea.getCompanyId());
        if (company.isPresent()) {
            Set<Customer> customers = customerRepository.findByAddressCityAndAddressState(company.get().getAddress().getCity(),
                    company.get().getAddress().getState());
            customers.forEach(cus -> {
                cus.getAddress().forEach(address -> {
                    if (checkLocationInRange(address, company.get())) {
                        SearchIndex searchIndex = new SearchIndex();
                        searchIndex.setCustomer(cus);
                        searchIndex.setCompany(company.get());
                        searchIndex.setAddress(address);
                        searchIndexRepository.save(searchIndex);
                    }
                });
            });
        }
    }

    private void createIndexFromUserAddress(Address userAddress, Long entityId) {
        List<Company> companyList = companyRepository.findByAddressCity(userAddress.getCity());
        companyList.forEach(mer -> {
            if (checkLocationInRange(userAddress, mer)) {
                SearchIndex searchIndex = new SearchIndex();
                Optional<Customer> customer = customerRepository.findById(entityId);
                Optional<Address> address = addressRepository.findById(userAddress.getId());
                if (customer.isPresent() && address.isPresent()) {
                    searchIndex.setCompany(mer);
                    searchIndex.setCustomer(customer.get());
                    searchIndex.setAddress(address.get());
                    searchIndexRepository.save(searchIndex);
                }
            }
        });
    }

    public boolean checkLocationInRange(Address address, Company company) {
        log.info("M checkLocationInRange");
        List<DeliveryArea> deliveryAreas = deliveryAreaRepository.findByCompanyOrderByLongitudeAsc(company);
        if (!deliveryAreas.isEmpty()) {
            Double[] topRight = getTopRigth(deliveryAreas);
            Double[] bottomLeft = getBottomLeft(deliveryAreas);
            return check(topRight, bottomLeft, new CoordinateDTO(Double.valueOf(address.getLongitude().toString()),
                    Double.valueOf(address.getLatitude().toString())));
        }
        log.warn("deliveryAreas is empty");
        return false;
    }

    private Double[] getBottomLeft(List<DeliveryArea> deliveryAreas) {
        log.debug("getBottomLeft");
        List<DeliveryArea> list = deliveryAreas;

        //gets the leftmost point
        list = list.stream().sorted(Comparator.comparing(DeliveryArea::getLongitude).reversed()).collect(Collectors.toList());
        Double[] res = new Double[2];
        res[0] = list.get(0).getLongitude();

        //gets the lower point
        list = list.stream().sorted(Comparator.comparing(DeliveryArea::getLatitude).reversed()).collect(Collectors.toList());
        res[1] = list.get(0).getLatitude();

        log.debug(Arrays.toString(res));
        return res;
    }

    private Double[] getTopRigth(List<DeliveryArea> deliveryAreas) {
        log.debug("getTopRigth");
        List<DeliveryArea> list = deliveryAreas;
        //gets the rightmost point
        list = list.stream().sorted(Comparator.comparing(DeliveryArea::getLongitude)).collect(Collectors.toList());

        Double[] res = new Double[2];
        res[0] = list.get(0).getLongitude();

        //gets the higher point
        list = list.stream().sorted(Comparator.comparing(DeliveryArea::getLatitude)).collect(Collectors.toList());
        res[1] = list.get(0).getLatitude();
        log.debug(Arrays.toString(res));
        return res;
    }

    private static boolean check(Double[] topRight, Double[] bottomLeft, CoordinateDTO userCoordinate) {
        // bottom-left and top-right
        // corners of rectangle
        Double x1 = topRight[0], y1 = topRight[1],
                x2 = bottomLeft[0], y2 = bottomLeft[1];

        // given point
        Double x = userCoordinate.getLongitude(), y = userCoordinate.getLatitude();

        // function call
        if (FindPoint(x1, y1, x2, y2, x, y)) {
            log.debug("In range: User Location " + userCoordinate.getLongitude() + " " + userCoordinate.getLatitude() +
                    " - Store delivery area: " + Arrays.toString(topRight) + " " + Arrays.toString(bottomLeft));
            return true;
        } else {
            log.warn("Not in range: User Location " + userCoordinate.getLongitude() + " " + userCoordinate.getLatitude() +
                    " - Store delivery area: " + Arrays.toString(topRight) + " " + Arrays.toString(bottomLeft));
            return false;
        }
    }

    static boolean FindPoint(Double x1, Double y1, Double x2,
                             Double y2, Double x, Double y) {
        if (x >= x1 && x <= x2 &&
                y >= y1 && y <= y2)
            return true;

        return false;
    }

    private void deletePreviousArea(DeliveryAreaRequest deliveryArea) {
        Optional<Company> company = companyRepository.findById(deliveryArea.getCompanyId());
        company.ifPresent(comp -> {
            if (searchIndexRepository.existsByCompany(comp)) {
                searchIndexRepository.deleteByCompany(comp);
            }
        });
    }

}
