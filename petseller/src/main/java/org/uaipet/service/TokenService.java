package org.uaipet.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.uaipet.data.model.Customer;
import org.uaipet.data.model.ValidationToken;
import org.uaipet.data.repository.ValidationTokenRepository;

import java.util.Random;

@Service
public class TokenService {

    private static final Logger log = LoggerFactory.getLogger(TokenService.class);

    @Autowired
    private ValidationTokenRepository validationTokenRepository;

    public ValidationToken generateToken(Customer customer){
        Random rnd = new Random();
        int number = rnd.nextInt(999999);
        // this will convert any number sequence into 6 character.
        String token = String.format("%06d", number);
        ValidationToken obj = new ValidationToken();
        obj.setToken(Integer.valueOf(token));
        obj.setUser(customer);
        return validationTokenRepository.save(obj);
    }

}
