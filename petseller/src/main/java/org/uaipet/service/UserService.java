package org.uaipet.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.uaipet.data.model.Company;
import org.uaipet.data.model.User;
import org.uaipet.data.repository.CompanyRepository;
import org.uaipet.data.repository.UserRepository;
import org.uaipet.exception.UserNotAssociatedWithCompanyException;
import org.uaipet.security.UserSS;
import org.uaipet.service.exceptions.AuthorizationException;

import java.util.Objects;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private CompanyRepository companyRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final UserRepository repository;

    @Autowired
    public UserService( UserRepository repository) {
        this.repository = repository;
    }

    @Transactional(rollbackFor = Exception.class)
    public User save(User user) {
        return this.repository.save(user);
    }

    @Transactional(readOnly = true)
    public User findByEmail(String email) {
        return this.repository.findByEmail(email);
    }

    @Transactional(readOnly = true)
    public Optional<User> findById(Long id) {
        return this.repository.findById(id);
    }

    public static UserSS authenticated() {
        try {
            return (UserSS) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        }
        catch (Exception e) {
            return null;
        }
    }

    public void requestUserIdEqualsTokenUserId(Long requestUserId){
        UserSS user = authenticated();
        if (Objects.isNull(user) ||  !Objects.equals(requestUserId, user.getId())) {
            throw new AuthorizationException("Acesso negado");
        }
    }

    public Boolean requestUserIsAssociatedWithCompany(Long userId, Long companyId){
        requestUserIdEqualsTokenUserId(userId);
        Optional<Company> merchant = companyRepository.findById(companyId);
        UserSS userCompany = authenticated();
        Optional<Boolean> response = merchant.map(mer -> {
                    if(Objects.isNull(userCompany) || !userCompany.getId().equals(userId)) {
                        throw new UserNotAssociatedWithCompanyException();
                    }
                    return true;
                });
        return response.get();
    }

}
