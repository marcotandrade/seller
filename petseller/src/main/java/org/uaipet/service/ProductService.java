package org.uaipet.service;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.*;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.multipart.MultipartFile;
import org.uaipet.dto.*;
import org.uaipet.dto.enums.MeasurementUnitEnum;
import org.uaipet.dto.enums.OrderStatusEnum;
import org.uaipet.dto.enums.TransactionStatus;
import org.uaipet.dto.request.ProductRequest;
import org.uaipet.exception.CustomerNotFoundException;
import org.uaipet.exception.ProductException;
import org.uaipet.data.model.*;
import org.uaipet.data.model.OrderStatus;
import org.uaipet.data.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.uaipet.security.UserSS;
import org.uaipet.service.exceptions.AddressException;
import org.uaipet.service.exceptions.AuthorizationException;
import org.uaipet.service.exceptions.OutOfDeliveryRangeException;
import org.uaipet.service.exceptions.ProductFromDifferentStoreException;
import org.uaipet.service.exceptions.ProductUnavailableException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Service
public class ProductService {

    private static final Logger log = LoggerFactory.getLogger(ProductService.class);

    @Value("${product.search.pageSize}")
    private int pageSize;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private ProductBasketRepository productBasketRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderService orderService;

    @Autowired
    private DeliveryAddressRepository deliveryAddressRepository;

    @Autowired
    private AnimalCategoryRepository animalCategoryRepository;

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private AgeCategoryRepository ageCategoryRepository;

    @Autowired
    private MeasurementCategoryRepository measurementCategoryRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private SearchIndexService searchIndexService;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private S3StorageService s3StorageService;

    @Autowired
    private SearchIndexRepository searchIndexRepository;

    @Autowired
    private UserService userService;


    public Product createProduct(ProductRequest productRequest, List<MultipartFile> images) throws ProductException {
        userService.requestUserIsAssociatedWithCompany(productRequest.getUserId(), productRequest.getCompanyId());
        try {
            Optional<Company> merchant = companyRepository.findById(productRequest.getCompanyId());
            Optional<AnimalCategory> animalCategory = animalCategoryRepository.findById(productRequest.getAnimalCategory());
            Optional<ProductCategory> productCategory = productCategoryRepository.findById(productRequest.getProductCategory());
            Optional<AgeCategory> ageCategory = productRequest.getAnimalAge() != null ?
                    ageCategoryRepository.findById(productRequest.getAnimalAge()) : Optional.empty();
            Optional<MeasurementCategory> measurementCategory = productRequest.getMeasurementUnit() != null ?
                    measurementCategoryRepository.findById(productRequest.getMeasurementUnit()) : Optional.empty();

            if(merchant.isPresent() && animalCategory.isPresent() && productCategory.isPresent()){
               Product p = Product.builder()
                        .productCategory(productCategory.get())
                        .animalCategory(animalCategory.get())
                        .company(merchant.get())
                        .name(productRequest.getName())
                        .price(productRequest.getPrice())
                        .measurementUnit(measurementCategory.orElse(null))
                        .ageCategory(ageCategory.orElse(null))
                        .measurementUnitValue(productRequest.getMeasurementUnitValue())
                        .manufacturer(productRequest.getManufacturer())
                        .stockCount(productRequest.getStockCount())
                        .description(productRequest.getDescription().getBytes(StandardCharsets.UTF_8))
                        .build();

                saveProductPicture(images, p);
                return productRepository.save(p);
            }
            throw new ProductException("Data not found!");
        } catch (Exception ex) {
            throw new ProductException(ex.getMessage());
        }
    }


    public void saveProductPicture(List<MultipartFile> images,
                                      Product product) throws ProductException {
        log.info(String.format("saveProductPicture"));
        if(!images.isEmpty() && product != null){
            try {
                ArrayList<String> imagesUrl = s3StorageService.uploadImageFile(images, product.getName()+"-"+ product.getId() + "-picture", "product-images/");
                product.setImage(imagesUrl);
            }catch (IOException ex){
                throw new ProductException("Error saving image from product - " + product.toString());
            }
        }
    }

    public List<Product> findProductByMerchant(Long merchantId) throws ProductException {
        try {
            return productRepository.findByCompanyIdAndStockCountGreaterThan(merchantId, 0L);
        } catch (Exception ex) {
            throw new ProductException(ex.getMessage());
        }
    }

    public List<ProductDTO> searchProducts(ProductSearchDTO search) throws ProductException {
        List<Product> pList = new ArrayList<>();

        if(search.getUserId() == null) {
            pList = genericSearch(search);
        } else {
            userService.requestUserIdEqualsTokenUserId(search.getUserId());

            List<SearchIndex> list = new ArrayList<>();
            Optional<Address> address;
            Optional<Customer> customer = customerRepository.findCustomerByUserId(search.getUserId());
            if (customer.isPresent()) {
                address = customer.get().getAddress().stream().filter(address1 -> address1.getDefaultAddress().equals(Boolean.TRUE)).findAny();
                if (address.isPresent()) {
                    list = searchIndexRepository.findByCustomerAndAddress(customer.get(), address.get());
                }
                Set<Company> companySet = new HashSet<>();
                list.forEach(item -> companySet.add(item.getCompany()));
                pList = findProducts(search, companySet);
            }
        }
        List<ProductDTO> resList = new ArrayList<>();
        if (!pList.isEmpty()) {
            for (Product prod : pList) {
                resList.add(ProductDTO.builder()
                        .id(prod.getId())
                        .name(prod.getName())
                        .productCategory(prod.getProductCategory())
                        .price(prod.getPrice())
                        .manufacturer(prod.getManufacturer())
                        .measurementUnitEnum(MeasurementUnitEnum.valueOf(prod.getMeasurementUnit().getName()))
                        .measurementUnitValue(prod.getMeasurementUnitValue())
                        .image(prod.getImage())
                        .company(CompanyDTO.builder()
                                .id(prod.getCompany().getId())
                                .nomeFantasia(prod.getCompany().getNomeFantasia())
                                .cnpj(prod.getCompany().getCnpj())
                                .freightValue(prod.getCompany().getFreightValue())
                                .build())
                        .build());
            }
        }
        return resList;
    }

    private  List<Product> genericSearch(ProductSearchDTO search) throws ProductException {

        return findProducts(search, new HashSet<Company>());
    }

    private List<Product> findProducts(ProductSearchDTO search, Set<Company> companyList) throws ProductException {
        try {
            Pageable pg = getPageable(search);
            if (companyList.isEmpty()) {
               // usuário não cadastrou endereço e não possui dados na tabela search_index
                ExampleMatcher matcher = ExampleMatcher
                        .matchingAll()
                        .withMatcher("name", contains().ignoreCase())
                        .withMatcher("animalCategory", exact())
                        .withMatcher("productCategory", exact());

                Product dynamicSearch = Product
                        .builder()
                        .animalCategory(search.getAnimalCategory() == null ? null : animalCategoryRepository.findById(search.getAnimalCategory()).orElse(null))
                        .productCategory(search.getProductCategory() == null ? null : productCategoryRepository.findById(search.getProductCategory()).orElse(null))
                        .name(search.getName())
                        .build();

                return productRepository.findAll(Example.of(dynamicSearch, matcher), pg).getContent();
            } else {
                //search index configurado
                if (search.getName() != null && search.getAnimalCategory() != null && search.getProductCategory() != null) {
                    Optional<AnimalCategory> animalCategory = animalCategoryRepository.findById(search.getAnimalCategory());
                    Optional<ProductCategory> productCategory = productCategoryRepository.findById(search.getProductCategory());
                    if (animalCategory.isPresent() && productCategory.isPresent()) {
                        return productRepository.findByNameContainingIgnoreCaseAndAnimalCategoryAndProductCategoryAndCompanyInAndStockCountGreaterThan(search.getName(),
                                animalCategory.get(), productCategory.get(), companyList, 0L, pg);
                    }
                }
                if (search.getAnimalCategory() != null && search.getProductCategory() != null) {
                    Optional<AnimalCategory> animalCategory = animalCategoryRepository.findById(search.getAnimalCategory());
                    Optional<ProductCategory> productCategory = productCategoryRepository.findById(search.getProductCategory());
                    if (animalCategory.isPresent() && productCategory.isPresent()) {
                        return productRepository.findByAnimalCategoryAndProductCategoryAndCompanyInAndStockCountGreaterThan(animalCategory.get(), productCategory.get(),companyList, 0L, pg);
                    }
                }
                if (search.getName() != null) {
                    return productRepository.findByNameContainingIgnoreCaseAndCompanyIn(search.getName(), companyList, pg);
                } else {
                    return productRepository.findAll(pg).getContent();
                }
            }
        } catch (Exception ex) {
            throw new ProductException(ex.getMessage());
        }
    }

    private Pageable getPageable(ProductSearchDTO search) {
        if (StringUtils.isNotBlank(search.getSortedBy()) && StringUtils.isNotBlank(search.getDirection())) {
            if (search.getDirection().equalsIgnoreCase("lowest")) {
                search.setDirection("asc");
            } else {
                search.setDirection("desc");
            }
            return PageRequest.of(search.getPage(), pageSize,
                    Sort.Direction.valueOf(search.getDirection().toUpperCase()), search.getSortedBy());
        }
        if (StringUtils.isNotBlank(search.getSortedBy())) {
            return PageRequest.of(search.getPage(), pageSize, Sort.by(search.getSortedBy()));
        } else {
            return PageRequest.of(search.getPage(), pageSize);
        }
    }

    public TransactionStatus buyProducts(PaymentRequestDTO request) throws ProductException {
        if (productAvailable(request) && productsFromSameStore(request) && checkDeliveryAddress(request) ) {
            try {
                Optional<Customer> customer = customerRepository.findCustomerByUserId(request.getUserId());
                if (isValidatedCustomer(customer)) {
                    //transaction payed on delivery
                    productCountDown(request);
                    saveUaiPetTransaction(request, customer.get());
                    return TransactionStatus.SUCCESS;
                } else {
                    log.warn("Usuario não encontrado para efetuar a compra!");
                    productCountDownRollback(request);
                    return TransactionStatus.FAIL;
                }
            } catch (Exception ex) {
                log.error(String.format("Compra recusada pelo pagseguro, motivo: %s ", ex));
                productCountDownRollback(request);
                throw new ProductException(ex, ex.getMessage());
            }
        } else {
            return TransactionStatus.FAIL;
        }
    }

    private boolean isValidatedCustomer(Optional<Customer> customer) {
        if(customer.isPresent()){
          return Objects.nonNull(customer.get().getValid()) && customer.get().getValid();
        }
        throw new CustomerNotFoundException();
    }

    public String updateStockCount(Long prodId, Long stockCount, Long merchantId) throws ProductException {
        Optional<Company> merchant = companyRepository.findById(merchantId);
        if(merchant.isPresent()){
            Product product = productRepository.findByIdAndCompany(prodId, merchant.get());
            if(product != null){
                product.setStockCount(stockCount);
                productRepository.save(product);
                return "Atualizado com sucesso";
            }
        }
        throw new ProductException("Produto e/ou Loja não encontrados para efetuar atualização");
    }

    //check fif the delivery address is on the realy on store delivery range
    private boolean checkDeliveryAddress(PaymentRequestDTO request) {
        log.debug("M checkDeliveryAddress");
        Optional<Boolean> response = Optional.empty();
        Optional<Customer> customer = customerRepository.findCustomerByUserId(request.getUserId());
        response = customer.map(cust -> {
            if(cust.getAddress().stream().anyMatch(ad -> ad.getId().equals(request.getShipAddressId()))){
                // id on request is a address from the user
                Optional<Address> userAddress =  addressRepository.findById(request.getShipAddressId());
                Optional<Product> product = productRepository.findById(request.getPList().get(0).getProductId());
                if(userAddress.isPresent() && product.isPresent()) {
                    Company company = product.get().getCompany();
                    if(!searchIndexService.checkLocationInRange(userAddress.get(), company)){
                        throw new OutOfDeliveryRangeException();
                    }else{
                        return true;
                    }
                }
            }else{
                 throw new AddressException("Este endereço não pertence ao usuário");
            }
            return true;
        });
       return response.get();
    }

    //validates if the list of products are from the same store
    private Boolean productsFromSameStore(PaymentRequestDTO request) {
        log.debug("M productsFromSameStore");
        Long merchantId = productRepository.findById(request.getPList().get(0).getProductId()).get().getCompany().getId();
        for(ProductQuantityDTO item : request.getPList()){
            if(!merchantId.equals(productRepository.findById(item.getProductId()).get().getCompany().getId())){
                throw new ProductFromDifferentStoreException();
            }
        }
        return true;
    }

    //count down the product stock before the purchase
    private void productCountDown(PaymentRequestDTO request) {
        log.debug("M productCountDown");
        for (ProductQuantityDTO prd : request.getPList()) {
            productRepository.countDownProductStockQtt(prd.getQuantity(), prd.getProductId());
        }
    }

    private void productCountDownRollback(PaymentRequestDTO request) {
        for (ProductQuantityDTO prd : request.getPList()) {
            productRepository.rollbackStockCountDown(prd.getQuantity(), prd.getProductId());
        }
    }


    //validates if the products are available
    private boolean productAvailable(PaymentRequestDTO request) {
        for (ProductQuantityDTO prd : request.getPList()) {
            boolean exists = productRepository.existsByIdAndStockCountGreaterThanEqual(prd.getProductId(), prd.getQuantity().longValue());
            if (!exists) {
                throw new ProductUnavailableException();
            }
        }
        return true;
    }

  //  @Transactional(propagation = Propagation.REQUIRES_NEW)
    public boolean saveUaiPetTransaction(PaymentRequestDTO request, Customer customer)  {
        Transaction tran = new Transaction();
        try {
            //UaiPet transaction
            tran.setDate(new Date());
            tran.setAmount(getProductAmount(request));
            tran.setCustomer(customer);
            tran.setCompany(productRepository.findById(request.getPList().get(0).getProductId()).get().getCompany());
            tran.setTransactionStatus(TransactionStatus.SUCCESS);

            //product Basket
            List<ProductBasket> products = new ArrayList<>();
            for (ProductQuantityDTO prodDTO : request.getPList()) {
                Optional<Product> prod = productRepository.findById(prodDTO.getProductId());
                if (prod.isPresent()) {
                    ProductBasket pBasket = new ProductBasket();
                    pBasket.setProduct(prod.get());
                    pBasket.setProductName(prod.get().getName());
                    pBasket.setProductPrice(prod.get().getPrice() * prodDTO.getQuantity());
                    pBasket.setProductQuantity(prodDTO.getQuantity());
                    products.add(pBasket);
                }
            }

            //purchase order
            OrderStatus od = new OrderStatus();
            od.setTransaction(tran);
            od.setOrderStatusEnum(OrderStatusEnum.WAITING_CONFIRMATION);

            //order delivery
            Optional<Address> userAddress = addressRepository.findById(request.getShipAddressId());


            tran.setProductBasket(products);
            Transaction transaction = transactionRepository.save(tran);

            od.setTransaction(transaction);
            OrderStatus orderStatus = orderRepository.save(od);

            DeliveryAddress deliveryAddress =
            DeliveryAddress.builder()
                    .address(userAddress.get())
                    .orderStatus(orderStatus)
                    .build();
            deliveryAddressRepository.save(deliveryAddress);

        } catch (Exception e) {
            tran.setTransactionStatus(TransactionStatus.FAIL);
            return false;
        }
        return true;
    }

    private double getProductAmount(PaymentRequestDTO request) {
        Double purchaseAmount = 0.00;
        for (ProductQuantityDTO productQuantity : request.getPList()) {
            Optional<Product> pr = productRepository.findById(productQuantity.getProductId());
            if (pr.isPresent()) {
                purchaseAmount = purchaseAmount + (pr.get().getPrice() * productQuantity.getQuantity());
            }
        }
        return purchaseAmount;
    }
}