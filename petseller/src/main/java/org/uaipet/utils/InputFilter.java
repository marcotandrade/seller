package org.uaipet.utils;

import org.springframework.stereotype.Service;

@Service
public class InputFilter {


    public boolean validateInput(String input, String validationRegex){
        return input.matches(validationRegex);
    }

    public static void main(String[] args) {

        String input = "LASKDJA = 3";
        Long num = 33L;

       boolean res = input.matches(RegularExpression.VALIDATE_WORDS_OR_NUMBERS.value);
       System.out.println(res);

    }



}
