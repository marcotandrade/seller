package org.uaipet.utils;

public enum RegularExpression {

    VALIDATE_CPF("[^w\\.\\,\\-\\<\\>\\=]{11}"),
    VALIDATE_CNPJ("\\d{14}"),
    VALIDATE_EMAIL("[^=<>,]+\\@[^\\d=<>,]+\\.com.*"),
    VALIDATE_MSISDN("[0-9]{11}"),
    VALIDATE_PASSWORD("[\\d]{6}"),
    VALIDATE_NAME("[^=<>]+"),
    VALIDATE_ONLY_NUMBER("[^\\.=<>\\D]+"),
    VALIDATE_ONLY_WORDS("[^\\&\\*\\(\\)\\+\\-\\=\\<\\>\\d\\_]+"),
    VALIDATE_WORDS_OR_NUMBERS("[^\\*\\-\\=\\<\\>]+"),
    VALIDATE_STOCK_COUNT("[1-9]{1}|[1-9]{2}|[1-9]{3}|[1-9]{4}|[1-9]{5}");

    public String value;

    RegularExpression(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
