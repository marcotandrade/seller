package org.uaipet.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.uaipet.data.model.Company;
import org.uaipet.dto.CustomerDTO;
import org.uaipet.dto.enums.RegistrationEnum;
import org.uaipet.dto.request.CustomerRequest;

import java.util.Objects;

@Component
public class Validation {

    @Autowired
    private InputFilter inputFilter;

    public boolean companyObjectInfo(Company company) {

        if(Objects.nonNull(company.getUsers().get(0))  && Objects.nonNull(company.getCnpj())) {
            boolean inputCnpj = inputFilter.validateInput(company.getCnpj(), RegularExpression.VALIDATE_CNPJ.value);
            boolean inputName = inputFilter.validateInput(company.getNomeFantasia(), RegularExpression.VALIDATE_WORDS_OR_NUMBERS.value);
            boolean inputEmail = inputFilter.validateInput(company.getUsers().get(0).getEmail(), RegularExpression.VALIDATE_EMAIL.value);

            return inputCnpj && inputName && inputEmail;
        }
        return false;
    }

    public RegistrationEnum custumerObjectInfo(CustomerRequest us) {
        if (StringUtils.isNotBlank(us.getExpoToken()) && StringUtils.isNotBlank(us.getFullName())
                && StringUtils.isNotBlank(us.getEmail()) && StringUtils.isNotBlank(us.getPassword())) {
            return RegistrationEnum.COMPLETE;

        } else if (StringUtils.isNotBlank(us.getExpoToken())) {
            return RegistrationEnum.INCOMPLETE;
        }
        return null;
    }
}
