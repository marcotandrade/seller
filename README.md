# Uai Pet Org

Marketplace voltado para o mercado de pets

### Estrutura
- Projeto Maven
- Java 8
- Spring Boot
- Api de pagamentos: PagSeguro
- Postgres (RDS)

#### Executando projeto
Basta executar a classe: PetselletApplication para iniciar o projeto.

#### Chamando Serviços expostos pela aplicação
No mesmo nível do arquivo pom.xml há um arquivo chamado "Pet_Seller_collection.json"
este contém exemplos de request que podem ser importado no postman
para testar os endpoints.




